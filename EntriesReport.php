<?php
/**
 * Entries Report
 *
 * @package Entry and Exit module
 */

require_once 'ProgramFunctions/TipMessage.fnc.php';
require_once 'modules/Entry_Exit/includes/common.fnc.php';
require_once 'modules/Entry_Exit/includes/EntriesReport.fnc.php';

DrawHeader( ProgramTitle() );

if ( empty( $_REQUEST['checkpoint_id'] ) )
{
	$default_checkpoint_id = DBGetOne( "SELECT ID
		FROM entry_exit_checkpoints
		ORDER BY SORT_ORDER IS NULL,SORT_ORDER
		LIMIT 1" );

	if ( ! $default_checkpoint_id )
	{
		$error[] = sprintf(
			_( 'No %s were found.' ),
			mb_strtolower( dngettext( 'Entry_Exit', 'Checkpoint', 'Checkpoints', 0 ) )
		);

		echo ErrorMessage( $error, 'fatal' );
	}

	$_REQUEST['checkpoint_id'] = $default_checkpoint_id;
}

$_REQUEST['return'] = RequestedDate( 'return', DBDate() );

echo '<form action="' . PreparePHP_SELF(
	[],
	[
		'checkpoint_id',
		'return',
		'month_return',
		'day_return',
		'year_return',
	]
) . '" method="GET">';

if ( ! AllowEdit() )
{
	$tmp_allow_edit = true;

	$_ROSARIO['allow_edit'] = true;
}

DrawHeader(
	EntryExitEntriesReportCheckpointHeader(),
	EntryExitEntriesReportDateHeader()
);

if ( ! empty( $tmp_allow_edit ) )
{
	$_ROSARIO['allow_edit'] = false;
}

echo '</form>';

// List students who have no record (in or out) for today before "Return Time".
$extra = EntryExitEntriesReportSearchExtra();

/*if ( empty( $_REQUEST['LO_sort'] ) )
{
	$_REQUEST['LO_sort'] = 'EVENING_LEAVE';
}*/

Search( 'student_id', $extra );
