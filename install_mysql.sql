/**
 * Install MySQL
 * Required if the module has menu entries
 * - Add profile exceptions for the module to appear in the menu
 * - Add program config options if any (to every schools)
 * - Add module specific tables (and their eventual sequences & indexes)
 *   if any: see rosariosis.sql file for examples
 *
 * @package Entry and Exit module
 */

/**
 * config Table
 *
 * syear: school year (school may have various years in DB)
 * school_id: may exists various schools in DB
 * program: convention is plugin name, for ex.: 'ldap'
 * title: for ex.: 'ENTRY_EXIT_[your_config]'
 * value: string
 */
--
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: rosariosis
--

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS', '365'
FROM DUAL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS');



/**
 * profile_exceptions Table
 *
 * profile_id:
 * - 0: student
 * - 1: admin
 * - 2: teacher
 * - 3: parent
 * modname: should match the Menu.php entries
 * can_use: 'Y'
 * can_edit: 'Y' or null (generally null for non admins)
 */
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/Checkpoints.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/Checkpoints.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/Records.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/Records.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 2, 'Entry_Exit/Records.php', 'Y', NULL
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/Records.php'
    AND profile_id=2);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/EveningLeave.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/EveningLeave.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 2, 'Entry_Exit/EveningLeave.php', 'Y', NULL
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/EveningLeave.php'
    AND profile_id=2);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/AddRecords.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/AddRecords.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/PackageDelivery.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/PackageDelivery.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/MassAddEveningLeaves.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/MassAddEveningLeaves.php'
    AND profile_id=1);


/**
 * Add module tables
 */

/**
 * Checkpoints table
 */
--
-- Name: entry_exit_checkpoints; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS entry_exit_checkpoints (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title varchar(100) NOT NULL,
    type varchar(3), -- 'in' or 'out' or NULL (both)
    authorized_from time,
    authorized_to time,
    sort_order numeric,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp
);



/**
 * Student Records table
 */
--
-- Name: entry_exit_student_records; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS entry_exit_student_records (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    student_id integer NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students(student_id),
    checkpoint_id integer NOT NULL,
    FOREIGN KEY (checkpoint_id) REFERENCES entry_exit_checkpoints(id) ON DELETE CASCADE,
    type varchar(3) NOT NULL, -- 'in' or 'out'
    unauthorized varchar(1), -- 'Y'
    comments varchar(255),
    recorded_at timestamp NOT NULL,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp
);



--
-- Name: entry_exit_student_records_ind2; Type: INDEX; Schema: public; Owner: rosariosis; Tablespace:
--

DELIMITER $$
CREATE PROCEDURE create_index_entry_exit_student_records_ind2()
BEGIN
    DECLARE index_exists integer DEFAULT 0;

    SELECT count(1) INTO index_exists
    FROM information_schema.STATISTICS
    WHERE table_schema=DATABASE()
    AND table_name='entry_exit_student_records'
    AND index_name='entry_exit_student_records_ind2';

    IF index_exists=0 THEN
        CREATE INDEX entry_exit_student_records_ind2 ON entry_exit_student_records (type);
    END IF;
END $$
DELIMITER ;

CALL create_index_entry_exit_student_records_ind2();
DROP PROCEDURE create_index_entry_exit_student_records_ind2;


/**
 * Staff Records table
 */
--
-- Name: entry_exit_staff_records; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS entry_exit_staff_records (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    staff_id integer NOT NULL,
    FOREIGN KEY (staff_id) REFERENCES staff(staff_id),
    checkpoint_id integer NOT NULL,
    FOREIGN KEY (checkpoint_id) REFERENCES entry_exit_checkpoints(id) ON DELETE CASCADE,
    type varchar(3) NOT NULL, -- 'in' or 'out'
    unauthorized varchar(1), -- 'Y'
    comments varchar(255),
    recorded_at timestamp NOT NULL,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp
);



--
-- Name: entry_exit_staff_records_ind2; Type: INDEX; Schema: public; Owner: rosariosis; Tablespace:
--

DELIMITER $$
CREATE PROCEDURE create_index_entry_exit_staff_records_ind2()
BEGIN
    DECLARE index_exists integer DEFAULT 0;

    SELECT count(1) INTO index_exists
    FROM information_schema.STATISTICS
    WHERE table_schema=DATABASE()
    AND table_name='entry_exit_staff_records'
    AND index_name='entry_exit_staff_records_ind2';

    IF index_exists=0 THEN
        CREATE INDEX entry_exit_staff_records_ind2 ON entry_exit_staff_records (type);
    END IF;
END $$
DELIMITER ;

CALL create_index_entry_exit_staff_records_ind2();
DROP PROCEDURE create_index_entry_exit_staff_records_ind2;



/**
 * Package deliveries table
 */
--
-- Name: entry_exit_package_deliveries; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS entry_exit_package_deliveries (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    student_id integer NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students(student_id),
    comments varchar(255),
    delivered_at timestamp NOT NULL,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp
);



/**
 * Evening leaves table
 */
--
-- Name: entry_exit_evening_leaves; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS entry_exit_evening_leaves (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    syear numeric(4,0) NOT NULL,
    student_id integer NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students(student_id),
    checkpoint_id integer NOT NULL,
    FOREIGN KEY (checkpoint_id) REFERENCES entry_exit_checkpoints(id) ON DELETE CASCADE,
    from_date date NOT NULL,
    to_date date NOT NULL,
    days_of_week varchar(7) NOT NULL, -- UMTWHFS
    return_time time NOT NULL,
    comments varchar(255),
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp,
    created_by text
);



--
-- Name: entry_exit_evening_leaves_ind; Type: INDEX; Schema: public; Owner: rosariosis; Tablespace:
--

DELIMITER $$
CREATE PROCEDURE create_index_entry_exit_evening_leaves_ind()
BEGIN
    DECLARE index_exists integer DEFAULT 0;

    SELECT count(1) INTO index_exists
    FROM information_schema.STATISTICS
    WHERE table_schema=DATABASE()
    AND table_name='entry_exit_evening_leaves'
    AND index_name='entry_exit_evening_leaves_ind';

    IF index_exists=0 THEN
        CREATE INDEX entry_exit_evening_leaves_ind ON entry_exit_evening_leaves (syear,student_id,checkpoint_id);
    END IF;
END $$
DELIMITER ;

CALL create_index_entry_exit_evening_leaves_ind();
DROP PROCEDURE create_index_entry_exit_evening_leaves_ind;



/**
 * Student Field category (tab)
 * Include modules/Entry_Exit/Student.inc.php file.
 */
INSERT INTO student_field_categories
VALUES (NULL, 'Entry and Exit', NULL, NULL, 'Entry_Exit/Student', NULL, NULL);

SET @sfc_id = LAST_INSERT_ID();

/**
 * Profile exceptions
 * Give access to tab to all profiles (only admin can edit).
 */
INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '1',CONCAT('Students/Student.php&category_id=', @sfc_id),'Y','Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname=CONCAT('Students/Student.php&category_id=', @sfc_id)
    AND profile_id=1);

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '2',CONCAT('Students/Student.php&category_id=', @sfc_id),'Y',NULL
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname=CONCAT('Students/Student.php&category_id=', @sfc_id)
    AND profile_id=2);

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '3',CONCAT('Students/Student.php&category_id=', @sfc_id),'Y',NULL
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname=CONCAT('Students/Student.php&category_id=', @sfc_id)
    AND profile_id=3);

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '0',CONCAT('Students/Student.php&category_id=', @sfc_id),'Y',NULL
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname=CONCAT('Students/Student.php&category_id=', @sfc_id)
    AND profile_id=0);
