/**
 * Install PostgreSQL
 * Required if the module has menu entries
 * - Add profile exceptions for the module to appear in the menu
 * - Add program config options if any (to every schools)
 * - Add module specific tables (and their eventual sequences & indexes)
 *   if any: see rosariosis.sql file for examples
 *
 * @package Entry and Exit module
 */

-- Fix #102 error language "plpgsql" does not exist
-- http://timmurphy.org/2011/08/27/create-language-if-it-doesnt-exist-in-postgresql/
--
-- Name: create_language_plpgsql(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION create_language_plpgsql()
RETURNS BOOLEAN AS $$
    CREATE LANGUAGE plpgsql;
    SELECT TRUE;
$$ LANGUAGE SQL;

SELECT CASE WHEN NOT (
    SELECT TRUE AS exists FROM pg_language
    WHERE lanname='plpgsql'
    UNION
    SELECT FALSE AS exists
    ORDER BY exists DESC
    LIMIT 1
) THEN
    create_language_plpgsql()
ELSE
    FALSE
END AS plpgsql_created;

DROP FUNCTION create_language_plpgsql();


/**
 * config Table
 *
 * syear: school year (school may have various years in DB)
 * school_id: may exists various schools in DB
 * program: convention is plugin name, for ex.: 'ldap'
 * title: for ex.: 'ENTRY_EXIT_[your_config]'
 * value: string
 */
--
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: rosariosis
--

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS', '365'
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS');



/**
 * profile_exceptions Table
 *
 * profile_id:
 * - 0: student
 * - 1: admin
 * - 2: teacher
 * - 3: parent
 * modname: should match the Menu.php entries
 * can_use: 'Y'
 * can_edit: 'Y' or null (generally null for non admins)
 */
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/Checkpoints.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/Checkpoints.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/Records.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/Records.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 2, 'Entry_Exit/Records.php', 'Y', NULL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/Records.php'
    AND profile_id=2);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/EveningLeave.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/EveningLeave.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 2, 'Entry_Exit/EveningLeave.php', 'Y', NULL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/EveningLeave.php'
    AND profile_id=2);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/AddRecords.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/AddRecords.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/PackageDelivery.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/PackageDelivery.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Entry_Exit/MassAddEveningLeaves.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Entry_Exit/MassAddEveningLeaves.php'
    AND profile_id=1);


/**
 * Add module tables
 */

/**
 * Checkpoints table
 */
--
-- Name: entry_exit_checkpoints; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_entry_exit_checkpoints() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname=CURRENT_SCHEMA()
        AND tablename='entry_exit_checkpoints') THEN
    RAISE NOTICE 'Table "entry_exit_checkpoints" already exists.';
    ELSE
        CREATE TABLE entry_exit_checkpoints (
            id serial PRIMARY KEY,
            title varchar(100) NOT NULL,
            type varchar(3), -- 'in' or 'out' or NULL (both)
            authorized_from time,
            authorized_to time,
            sort_order numeric,
            created_at timestamp DEFAULT current_timestamp,
            updated_at timestamp
        );

        CREATE TRIGGER set_updated_at
            BEFORE UPDATE ON entry_exit_checkpoints
            FOR EACH ROW EXECUTE PROCEDURE set_updated_at();
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_entry_exit_checkpoints();
DROP FUNCTION create_table_entry_exit_checkpoints();


/**
 * Student Records table
 */
--
-- Name: entry_exit_student_records; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_entry_exit_student_records() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname=CURRENT_SCHEMA()
        AND tablename='entry_exit_student_records') THEN
    RAISE NOTICE 'Table "entry_exit_student_records" already exists.';
    ELSE
        CREATE TABLE entry_exit_student_records (
            id serial PRIMARY KEY,
            student_id integer NOT NULL REFERENCES students(student_id),
            checkpoint_id integer NOT NULL REFERENCES entry_exit_checkpoints(id) ON DELETE CASCADE,
            type varchar(3) NOT NULL, -- 'in' or 'out'
            unauthorized varchar(1), -- 'Y'
            comments varchar(255),
            recorded_at timestamp NOT NULL,
            created_at timestamp DEFAULT current_timestamp,
            updated_at timestamp
        );

        CREATE TRIGGER set_updated_at
            BEFORE UPDATE ON entry_exit_student_records
            FOR EACH ROW EXECUTE PROCEDURE set_updated_at();
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_entry_exit_student_records();
DROP FUNCTION create_table_entry_exit_student_records();



--
-- Name: entry_exit_student_records_ind; Type: INDEX; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_index_entry_exit_student_records_ind() RETURNS void AS
$func$
BEGIN
    IF NOT EXISTS (
        SELECT 1 FROM pg_class c
        JOIN pg_namespace n ON n.oid=c.relnamespace
        WHERE c.relname='entry_exit_student_records_ind'
        AND n.nspname=CURRENT_SCHEMA
    ) THEN
        CREATE INDEX entry_exit_student_records_ind ON entry_exit_student_records USING btree (checkpoint_id);
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_index_entry_exit_student_records_ind();
DROP FUNCTION create_index_entry_exit_student_records_ind();



--
-- Name: entry_exit_student_records_ind2; Type: INDEX; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_index_entry_exit_student_records_ind2() RETURNS void AS
$func$
BEGIN
    IF NOT EXISTS (
        SELECT 1 FROM pg_class c
        JOIN pg_namespace n ON n.oid=c.relnamespace
        WHERE c.relname='entry_exit_student_records_ind2'
        AND n.nspname=CURRENT_SCHEMA
    ) THEN
        CREATE INDEX entry_exit_student_records_ind2 ON entry_exit_student_records USING btree (type);
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_index_entry_exit_student_records_ind2();
DROP FUNCTION create_index_entry_exit_student_records_ind2();


/**
 * Staff Records table
 */
--
-- Name: entry_exit_staff_records; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_entry_exit_staff_records() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname=CURRENT_SCHEMA()
        AND tablename='entry_exit_staff_records') THEN
    RAISE NOTICE 'Table "entry_exit_staff_records" already exists.';
    ELSE
        CREATE TABLE entry_exit_staff_records (
            id serial PRIMARY KEY,
            staff_id integer NOT NULL REFERENCES staff(staff_id),
            checkpoint_id integer NOT NULL REFERENCES entry_exit_checkpoints(id) ON DELETE CASCADE,
            type varchar(3) NOT NULL, -- 'in' or 'out'
            unauthorized varchar(1), -- 'Y'
            comments varchar(255),
            recorded_at timestamp NOT NULL,
            created_at timestamp DEFAULT current_timestamp,
            updated_at timestamp
        );

        CREATE TRIGGER set_updated_at
            BEFORE UPDATE ON entry_exit_staff_records
            FOR EACH ROW EXECUTE PROCEDURE set_updated_at();
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_entry_exit_staff_records();
DROP FUNCTION create_table_entry_exit_staff_records();



--
-- Name: entry_exit_staff_records_ind; Type: INDEX; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_index_entry_exit_staff_records_ind() RETURNS void AS
$func$
BEGIN
    IF NOT EXISTS (
        SELECT 1 FROM pg_class c
        JOIN pg_namespace n ON n.oid=c.relnamespace
        WHERE c.relname='entry_exit_staff_records_ind'
        AND n.nspname=CURRENT_SCHEMA
    ) THEN
        CREATE INDEX entry_exit_staff_records_ind ON entry_exit_staff_records USING btree (checkpoint_id);
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_index_entry_exit_staff_records_ind();
DROP FUNCTION create_index_entry_exit_staff_records_ind();



--
-- Name: entry_exit_staff_records_ind2; Type: INDEX; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_index_entry_exit_staff_records_ind2() RETURNS void AS
$func$
BEGIN
    IF NOT EXISTS (
        SELECT 1 FROM pg_class c
        JOIN pg_namespace n ON n.oid=c.relnamespace
        WHERE c.relname='entry_exit_staff_records_ind2'
        AND n.nspname=CURRENT_SCHEMA
    ) THEN
        CREATE INDEX entry_exit_staff_records_ind2 ON entry_exit_staff_records USING btree (type);
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_index_entry_exit_staff_records_ind2();
DROP FUNCTION create_index_entry_exit_staff_records_ind2();


/**
 * Package deliveries table
 */
--
-- Name: entry_exit_package_deliveries; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_entry_exit_package_deliveries() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname=CURRENT_SCHEMA()
        AND tablename='entry_exit_package_deliveries') THEN
    RAISE NOTICE 'Table "entry_exit_package_deliveries" already exists.';
    ELSE
        CREATE TABLE entry_exit_package_deliveries (
            id serial PRIMARY KEY,
            student_id integer NOT NULL REFERENCES students(student_id),
            comments varchar(255),
            delivered_at timestamp NOT NULL,
            created_at timestamp DEFAULT current_timestamp,
            updated_at timestamp
        );

        CREATE TRIGGER set_updated_at
            BEFORE UPDATE ON entry_exit_package_deliveries
            FOR EACH ROW EXECUTE PROCEDURE set_updated_at();
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_entry_exit_package_deliveries();
DROP FUNCTION create_table_entry_exit_package_deliveries();



--
-- Name: entry_exit_package_deliveries_ind; Type: INDEX; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_index_entry_exit_package_deliveries_ind() RETURNS void AS
$func$
BEGIN
    IF NOT EXISTS (
        SELECT 1 FROM pg_class c
        JOIN pg_namespace n ON n.oid=c.relnamespace
        WHERE c.relname='entry_exit_package_deliveries_ind'
        AND n.nspname=CURRENT_SCHEMA
    ) THEN
        CREATE INDEX entry_exit_package_deliveries_ind ON entry_exit_package_deliveries USING btree (student_id);
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_index_entry_exit_package_deliveries_ind();
DROP FUNCTION create_index_entry_exit_package_deliveries_ind();


/**
 * Evening leaves table
 */
--
-- Name: entry_exit_evening_leaves; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_entry_exit_evening_leaves() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname=CURRENT_SCHEMA()
        AND tablename='entry_exit_evening_leaves') THEN
    RAISE NOTICE 'Table "entry_exit_evening_leaves" already exists.';
    ELSE
        CREATE TABLE entry_exit_evening_leaves (
            id serial PRIMARY KEY,
            syear numeric(4,0) NOT NULL,
            student_id integer NOT NULL REFERENCES students(student_id),
            checkpoint_id integer NOT NULL REFERENCES entry_exit_checkpoints(id) ON DELETE CASCADE,
            from_date date NOT NULL,
            to_date date NOT NULL,
            days_of_week varchar(7) NOT NULL, -- UMTWHFS
            return_time time NOT NULL,
            comments varchar(255),
            created_at timestamp DEFAULT current_timestamp,
            updated_at timestamp,
            created_by text
        );

        CREATE TRIGGER set_updated_at
            BEFORE UPDATE ON entry_exit_evening_leaves
            FOR EACH ROW EXECUTE PROCEDURE set_updated_at();
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_entry_exit_evening_leaves();
DROP FUNCTION create_table_entry_exit_evening_leaves();



--
-- Name: entry_exit_evening_leaves_ind; Type: INDEX; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_index_entry_exit_evening_leaves_ind() RETURNS void AS
$func$
BEGIN
    IF NOT EXISTS (
        SELECT 1 FROM pg_class c
        JOIN pg_namespace n ON n.oid=c.relnamespace
        WHERE c.relname='entry_exit_evening_leaves_ind'
        AND n.nspname=CURRENT_SCHEMA
    ) THEN
        CREATE INDEX entry_exit_evening_leaves_ind ON entry_exit_evening_leaves USING btree (syear,student_id,checkpoint_id);
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_index_entry_exit_evening_leaves_ind();
DROP FUNCTION create_index_entry_exit_evening_leaves_ind();



/**
 * Student Field category (tab)
 * Include modules/Entry_Exit/Student.inc.php file.
 */
INSERT INTO student_field_categories
VALUES (nextval('student_field_categories_id_seq'), 'Entry and Exit', NULL, NULL, 'Entry_Exit/Student');

/**
 * Profile exceptions
 * Give access to tab to all profiles (only admin can edit).
 */
INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '1',CONCAT('Students/Student.php&category_id=', currval('student_field_categories_id_seq')),'Y','Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname=CONCAT('Students/Student.php&category_id=', currval('student_field_categories_id_seq'))
    AND profile_id=1);

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '2',CONCAT('Students/Student.php&category_id=', currval('student_field_categories_id_seq')),'Y',NULL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname=CONCAT('Students/Student.php&category_id=', currval('student_field_categories_id_seq'))
    AND profile_id=2);

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '3',CONCAT('Students/Student.php&category_id=', currval('student_field_categories_id_seq')),'Y',NULL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname=CONCAT('Students/Student.php&category_id=', currval('student_field_categories_id_seq'))
    AND profile_id=3);

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '0',CONCAT('Students/Student.php&category_id=', currval('student_field_categories_id_seq')),'Y',NULL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname=CONCAT('Students/Student.php&category_id=', currval('student_field_categories_id_seq'))
    AND profile_id=0);
