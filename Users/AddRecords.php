<?php
/**
 * Add Records (Users)
 *
 * URL to
 * - `&modfunc=save` = Save
 * - `&checkpoint_id=1` = Checkpoint ID 1
 * - `&record_type=in` = Record Type (entry: `in`, exit: `out`)
 * - `&unauthorized=auto` = Unauthorized (authorized: [empty], unauthorized: `Y`, automatic: `auto`)
 * - `&type=staff` = Type (students: `student`, users: `staff`)
 * - `&st[]=__STAFF_ID__` = User ID
 * - now
 * @example Modules.php?modname=Entry_Exit/AddRecords.php&modfunc=save&checkpoint_id=1&record_type=in&unauthorized=auto&type=staff&st[]=__STAFF_ID__
 *
 * @package Entry and Exit module
 */

if ( $_REQUEST['modfunc'] === 'save' )
{
	$recorded_at = RequestedDate( 'recorded_at', '' );

	if ( empty( $recorded_at ) )
	{
		$recorded_at = DBDate();

		$_REQUEST['recorded_at_time'] = date( 'H:i' );
	}

	if ( empty( $_REQUEST['recorded_at_time'] )
		|| strlen( $_REQUEST['recorded_at_time'] ) < 5 // 12:56
		|| empty( $_REQUEST['checkpoint_id'] ) )
	{
		$error[] = _( 'Please fill in the required fields' );
	}

	$recorded_at .= ' ' . $_REQUEST['recorded_at_time'];

	if ( empty( $_REQUEST['record_type'] ) )
	{
		$_REQUEST['record_type'] = DBGetOne( "SELECT TYPE
			FROM entry_exit_checkpoints
			WHERE ID='" . (int) $_REQUEST['checkpoint_id'] . "'" );

		if ( empty( $_REQUEST['record_type'] ) )
		{
			$last_record_type = EntryExitGetLastStaffRecordType( $checkpoint_id, $staff_id );

			$_REQUEST['record_type'] = $last_record_type === 'in' ? 'out' : 'in';
		}
	}

	if ( ! empty( $_REQUEST['unauthorized'] )
		&& $_REQUEST['unauthorized'] === 'auto' )
	{
		$unauthorized = EntryExitIsUnauthorized(
			$_REQUEST['checkpoint_id'],
			$_REQUEST['recorded_at_time']
		);

		$_REQUEST['unauthorized'] = $unauthorized ? 'Y' : '';
	}

	if ( empty( $_REQUEST['st'] ) )
	{
		$error[] = _( 'You must choose at least one user' );
	}

	if ( ! $error )
	{
		foreach ( (array) $_REQUEST['st'] as $staff_id )
		{
			if ( (int) $staff_id < 1 )
			{
				continue;
			}

			$inserted = DBInsert(
				'entry_exit_staff_records',
				[
					'STAFF_ID' => (int) $staff_id,
					'CHECKPOINT_ID' => (int) $_REQUEST['checkpoint_id'],
					'TYPE' => $_REQUEST['record_type'],
					'UNAUTHORIZED' => $_REQUEST['unauthorized'],
					'COMMENTS' => issetVal( $_REQUEST['comments'], '' ),
					'RECORDED_AT' => $recorded_at,
				]
			);
		}

		if ( ! empty( $inserted ) )
		{
			$note[] = button( 'check' ) . '&nbsp;' . dgettext( 'Entry_Exit', 'Records were added for the selected users.' );
		}
	}

	// Unset modfunc, st & redirect URL.
	RedirectURL( [ 'modfunc', 'st' ] );
}

echo ErrorMessage( $note, 'note' );

echo ErrorMessage( $error );

if ( ! $_REQUEST['modfunc'] )
{
	$extra['link'] = [ 'FULL_NAME' => false ];
	$extra['SELECT'] = ",NULL AS CHECKBOX";

	if ( $_REQUEST['search_modfunc'] === 'list' )
	{
		echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=save' ) . '" method="POST">';

		DrawHeader( '', SubmitButton( dgettext( 'Entry_Exit', 'Add Records to Selected Users' ) ) );

		echo '<br />';

		EntryExitAddRecordsPopTableOutput();

		echo '<br />';
	}

	$extra['functions'] = [ 'CHECKBOX' => 'MakeChooseCheckbox' ];

	$extra['columns_before'] = [
		'CHECKBOX' => MakeChooseCheckbox( 'required', 'STAFF_ID', 'st' ),
	];

	$extra['new'] = true;

	Search( 'staff_id', $extra );

	if ( $_REQUEST['search_modfunc'] === 'list' )
	{
		echo '<br /><div class="center">' .
			SubmitButton( dgettext( 'Entry_Exit', 'Add Records to Selected Users' ) ) . '</div></form>';
	}
}
