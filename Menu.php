<?php
/**
 * Menu.php file
 * Required
 * - Menu entries for the Entry and Exit module
 *
 * @package Entry and Exit module
 */

// @since 4.0 Add created_by column to entry_exit_evening_leaves table
require_once 'modules/Entry_Exit/includes/Update.inc.php';

// Menu entries for the Entry and Exit module.
$menu['Entry_Exit']['admin'] = [ // Admin menu.
	'title' => dgettext( 'Entry_Exit', 'Entry and Exit' ),
	'default' => 'Entry_Exit/Records.php', // Program loaded by default when menu opened.
	'Entry_Exit/Records.php' => dgettext( 'Entry_Exit', 'Records' ),
	'Entry_Exit/AddRecords.php' => dgettext( 'Entry_Exit', 'Add Records' ),
	'Entry_Exit/EntriesReport.php' => dgettext( 'Entry_Exit', 'Entries Report' ),
	'Entry_Exit/EveningLeave.php' => dgettext( 'Entry_Exit', 'Evening Leave' ),
	'Entry_Exit/MassAddEveningLeaves.php' => dgettext( 'Entry_Exit', 'Mass Add Evening Leaves' ),
	'Entry_Exit/PackageDelivery.php' => dgettext( 'Entry_Exit', 'Package Delivery' ),
	1 => _( 'Setup' ),
	'Entry_Exit/Checkpoints.php' => dgettext( 'Entry_Exit', 'Checkpoints' ),
	// Handle case when addon module activated BEFORE this module.
] + issetVal( $menu['Entry_Exit']['admin'], [] );

$menu['Entry_Exit']['teacher'] = [ // Teacher menu.
	'title' => dgettext( 'Entry_Exit', 'Entry and Exit' ),
	'default' => 'Entry_Exit/Records.php', // Program loaded by default when menu opened.
	'Entry_Exit/Records.php' => dgettext( 'Entry_Exit', 'Records' ),
	'Entry_Exit/EntriesReport.php' => dgettext( 'Entry_Exit', 'Entries Report' ),
	'Entry_Exit/EveningLeave.php' => dgettext( 'Entry_Exit', 'Evening Leave' ),
	'Entry_Exit/MassAddEveningLeaves.php' => dgettext( 'Entry_Exit', 'Mass Add Evening Leaves' ),
] + issetVal( $menu['Entry_Exit']['teacher'], [] );
