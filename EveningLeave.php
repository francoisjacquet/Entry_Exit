<?php
/**
 * Evening Leave report
 *
 * @package Entry and Exit module
 */

require_once 'ProgramFunctions/TipMessage.fnc.php';
require_once 'modules/Entry_Exit/includes/common.fnc.php';
require_once 'modules/Entry_Exit/includes/EveningLeaveReport.fnc.php';

DrawHeader( ProgramTitle() );

if ( empty( $_REQUEST['checkpoint_id'] ) )
{
	$default_checkpoint_id = DBGetOne( "SELECT ID
		FROM entry_exit_checkpoints
		ORDER BY SORT_ORDER IS NULL,SORT_ORDER
		LIMIT 1" );

	if ( ! $default_checkpoint_id )
	{
		$error[] = sprintf(
			_( 'No %s were found.' ),
			mb_strtolower( dngettext( 'Entry_Exit', 'Checkpoint', 'Checkpoints', 0 ) )
		);

		echo ErrorMessage( $error, 'fatal' );
	}

	$_REQUEST['checkpoint_id'] = $default_checkpoint_id;
}

$_REQUEST['return'] = RequestedDate( 'return', DBDate() );

if ( empty( $_REQUEST['return_time'] ) )
{
	$default_return_time = DBGetOne( "SELECT AUTHORIZED_TO
		FROM entry_exit_checkpoints
		WHERE ID='" . $_REQUEST['checkpoint_id'] . "'" );

	if ( $default_return_time )
	{
		$_REQUEST['return_time'] = $default_return_time;
	}
	else
	{
		$_REQUEST['return_time'] = '20:00';
	}
}
else
{
	Config( 'ENTRY_EXIT_EVENING_LEAVE_RETURN_TIME', $_REQUEST['return_time'] );
}

$_REQUEST['record_type'] = empty( $_REQUEST['record_type'] ) ? 'in' : $_REQUEST['record_type'];

$_REQUEST['hide_students_without_evening_leave'] = issetVal( $_REQUEST['hide_students_without_evening_leave'], '' );

// Advanced Search
if ( $_REQUEST['modfunc'] === 'search' )
{
	echo '<br />';

	$extra['new'] = true;

	$extra['search_title'] = _( 'Advanced' );

	$extra_action_url = PreparePHP_SELF( [], [ 'modfunc' ], [ 'advanced_search' => 1 ] );

	$extra['action'] = str_replace(
		'Modules.php?modname=' . $_REQUEST['modname'],
		'',
		$extra_action_url
	) . '&modfunc=&search_modfunc=';

	Search( 'student_id', $extra );
}
elseif ( ! $_REQUEST['modfunc'] )
{
	echo '<form action="' . PreparePHP_SELF(
		[],
		[
			'checkpoint_id',
			'return',
			'month_return',
			'day_return',
			'year_return',
			'return_time',
			'record_type',
			'hide_students_without_evening_leave',
		]
	) . '" method="GET">';

	if ( ! AllowEdit() )
	{
		$tmp_allow_edit = true;

		$_ROSARIO['allow_edit'] = true;
	}

	DrawHeader(
		EntryExitEveningLeaveCheckpointHeader(),
		EntryExitEveningLeaveDateHeader()
	);

	DrawHeader(
		EntryExitEveningLeaveTypeHeader(),
		EntryExitEveningLeaveReturnTimeHeader() . Buttons( _( 'Go' ) )
	);

	// Advanced Search link.
	$advanced_link = ' <a href="' . PreparePHP_SELF( $_REQUEST, [ 'search_modfunc', 'include_inactive' ], [
		'modfunc' => 'search',
	] ) . '">' . _( 'Advanced' ) . '</a>';

	DrawHeader(
		EntryExitEveningLeaveHideHeader(),
		$advanced_link
	);

	if ( ! empty( $tmp_allow_edit ) )
	{
		$_ROSARIO['allow_edit'] = false;
	}

	echo '</form>';

	// List students who have no record (in or out) for today before "Return Time".
	$extra = EntryExitEveningLeaveSearchExtra();

	$_REQUEST['search_modfunc'] = 'list';

	if ( empty( $_REQUEST['LO_sort'] ) )
	{
		$_REQUEST['LO_sort'] = 'EVENING_LEAVE';
	}

	Search( 'student_id', $extra );

	DrawHeader(
		EntryExitEveningLeaveStatusLegendHeader()
	);
}
