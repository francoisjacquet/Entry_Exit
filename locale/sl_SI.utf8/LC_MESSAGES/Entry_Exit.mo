��    $      <  5   \      0     1      =     ^     |     �  /   �  
   �     �  &   �       $   $     I     O     ^     l     {     �     �     �     �     �  2   �  M      F   n     �     �     �  -   �  *         +     7     <     A     N  "   k    �     �  "   �  &   �     �     �  .   	     ;	     L	  "   ]	     �	      �	     �	  	   �	     �	     �	     �	     �	     
     &
     6
     G
  -   N
  A   |
  ?   �
     �
            (     ,   C     p     }     �     �     �     �           $          #                     	               "                                                                                   !       
                                 Add Records Add Records to Selected Students Add Records to Selected Users Authorized From Authorized To Automatically delete records older than %s days Checkpoint Checkpoints Delete the selected Package Deliveries Delete the selected Records Deliver Package to Selected Students Entry Entry and Exit Evening Leave Evening Leaves Exit Has a package to pickup Only show last record Package Deliveries Package Deliveries History Package Delivery Packages were delivered for the selected students. Please enter an "Authorized To" time posterior to the "Authorized From" time. Please enter both the "Authorized From" and the "Authorized To" times. Private Notes Record Records Records were added for the selected students. Records were added for the selected users. Return Time Time Type Unauthorized You have a package to pickup Your child has a package to pickup Project-Id-Version: Entry and Exit module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-12-13 14:20+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.4.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Dodaj zapis Dodajanje zapisov izbranim dijakom Dodajanje zapisov izbranim uporabnikom Pooblaščeno od Pooblaščeno Za Samodejno izbriši zapise, starejše od %s dni Kontrolna točka Kontrolne točke Izbrišite izbrane paketne dostave Izbrišite izbrane zapise Dostavite paket izbranim dijakom Vhod Recepcija Večerni izhod Večerni izhodi Izhod Na recepciji ga čaka pošta Pokaži samo zadnji zapis Paketne dostave Zgodovina pošte Pošta Za izbrane dijake so bili dostavljeni paketi. Vnesite čas »Pooblaščeno do« za časom »Pooblaščeno od«. Prosimo, vnesite čas "Pooblaščeno od" in "Pooblaščeno do". Osebni zapiski Zapis Zapisi Za izbrane dijake so bili dodani zapisi. Za izbrane uporabnike so bili dodani zapisi. Čas vrnitve Čas Tip Nepooblaščeno Imate paket za prevzem Vaš otrok mora prevzeti paket 