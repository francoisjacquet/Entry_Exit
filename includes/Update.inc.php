<?php
/**
 * Update database
 *
 * @package Entry and Exit module
 */

global $DatabaseType;

// @since 4.0 Add created_by column to entry_exit_evening_leaves table
$created_by_column_exists = DBGetOne( "SELECT 1
	FROM information_schema.columns
	WHERE table_schema=" . ( $DatabaseType === 'mysql' ? 'DATABASE()' : 'CURRENT_SCHEMA()' ) . "
	AND table_name='entry_exit_evening_leaves'
	AND column_name='created_by';" );

$update_sql_file = ( ! empty( $DatabaseType ) && $DatabaseType === 'mysql' ?
	'modules/Entry_Exit/update4.0.sql' : // Same file for MySQL
	'modules/Entry_Exit/update4.0.sql' );

if ( ! $created_by_column_exists
	&& file_exists( $update_sql_file ) )
{
	// SQL Update to 4.0.
	$update_sql = file_get_contents( $update_sql_file );

	db_query( $update_sql );
}
