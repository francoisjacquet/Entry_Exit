<?php
/**
 * Package Deliveries History (Report)
 *
 * @package Entry and Exit module
 */

if ( AllowEdit()
	&& $_REQUEST['modfunc'] === 'delete' )
{
	// Prompt before deleting.
	if ( DeletePrompt( dgettext( 'Entry_Exit', 'Package Deliveries' ) ) )
	{
		if ( ! empty( $_REQUEST['pd'] ) )
		{
			$package_deliveries_list = implode( ',', array_map( 'intval', $_REQUEST['pd'] ) );

			DBQuery( "DELETE FROM entry_exit_package_deliveries
				WHERE ID IN(" . $package_deliveries_list . ")" );
		}

		// Unset modfunc & redirect URL.
		RedirectURL( [ 'modfunc', 'pd' ] );
	}
}

// Defaults to today.
$_REQUEST['start'] = RequestedDate( 'start', DBDate() );

$_REQUEST['end'] = RequestedDate( 'end', '' );

// Advanced Search
if ( $_REQUEST['modfunc'] === 'search' )
{
	echo '<br />';

	$extra['new'] = true;

	$extra['search_title'] = _( 'Advanced' );

	$extra_action_url = PreparePHP_SELF( [], [ 'modfunc' ], [ 'advanced_search' => 1 ] );

	$extra['action'] = str_replace(
		'Modules.php?modname=' . $_REQUEST['modname'],
		'',
		$extra_action_url
	) . '&modfunc=&search_modfunc=';

	Search( 'student_id', $extra );
}
elseif ( ! $_REQUEST['modfunc'] )
{
	echo '<form action="' . PreparePHP_SELF(
		[],
		[
			'start',
			'month_start',
			'day_start',
			'year_start',
			'end',
			'month_end',
			'day_end',
			'year_end',
		]
	) . '" method="GET">';

	if ( ! AllowEdit() )
	{
		$tmp_allow_edit = true;

		$_ROSARIO['allow_edit'] = true;
	}

	DrawHeader( EntryExitPackageDeliveryHistoryStartEndDateHeader() );

	// Advanced Search link.
	$advanced_link = ' <a href="' . PreparePHP_SELF( $_REQUEST, [ 'search_modfunc', 'include_inactive' ], [
		'modfunc' => 'search',
	] ) . '">' . _( 'Advanced' ) . '</a>';

	$records_sql = EntryExitPackageDeliveryHistorySQL( $_REQUEST['start'], $_REQUEST['end'] );

	DrawHeader(
		! empty( $_ROSARIO['SearchTerms'] ) ? $_ROSARIO['SearchTerms'] : '',
		$advanced_link
	);

	if ( ! empty( $tmp_allow_edit ) )
	{
		$_ROSARIO['allow_edit'] = false;
	}

	echo '</form>';

	$columns = [];

	if ( AllowEdit()
		&& ! isset( $_REQUEST['_ROSARIO_PDF'] ) )
	{
		$columns += [ 'CHECKBOX' => MakeChooseCheckbox( '', 'ID', 'pd' ) ];
	}

	$columns += [
		'DELIVERED_AT' => dgettext( 'Entry_Exit', 'Time' ),
		'FULL_NAME' => _( 'Student' ),
		'STUDENT_ID' => sprintf( _( '%s ID' ), Config( 'NAME' ) ),
		'GRADE_ID' => _( 'Grade Level' ),
		'COMMENTS' => _( 'Comments' ),
	];

	// SQL limit records to 1000.
	$records_RET = DBGet( $records_sql,
	[
		'CHECKBOX' => 'MakeChooseCheckbox',
		'DELIVERED_AT' => 'EntryExitMakeDateTime',
		'FULL_NAME' => 'makePhotoTipMessage',
		'GRADE_ID' => 'GetGrade',
		'COMMENTS' => 'EntryExitPackageDeliveryHistoryMakeComments',
	] );

	$options['pagination'] = true;

	echo '<form method="POST" action="' . PreparePHP_SELF( [], [], [ 'modfunc' => 'delete' ] ) . '">';

	ListOutput(
		$records_RET,
		$columns,
		dgettext( 'Entry_Exit', 'Record' ),
		dgettext( 'Entry_Exit', 'Package Deliveries' ),
		[],
		[],
		$options
	);

	if ( AllowEdit()
		&& $records_RET )
	{
		echo '<br /><div class="center">' .
			SubmitButton( dgettext( 'Entry_Exit', 'Delete the selected Package Deliveries' ), '', '' ) . '</div>';
	}

	echo '</form>';
}
