<?php
/**
 * Add Records functions
 *
 * @package Entry and Exit module
 */

function EntryExitPackageDeliveryPopTableOutput()
{
	PopTable( 'header', dgettext( 'Entry_Exit', 'Package Delivery' ) );

	echo '<table class="cellpadding-5">';

	echo '<tr class="st"><td>' . DateInput(
		DBDate(),
		'delivered_at',
		_( 'Date' ),
		false,
		true,
		true
	) . '</td>';

	echo '<td>' . TextInput(
		'',
		'delivered_at_time',
		dgettext( 'Entry_Exit', 'Time' ),
		'required type="time"'
	) . '</td></tr>';

	echo '<tr class="st"><td colspan="2">' . TextInput(
		'',
		'comments',
		_( 'Comments' ),
		'maxlength="100" size="30"'
	) . '</td></tr>';

	echo '</table>';

	PopTable( 'footer' );
}


/**
 * Package Delivery Widget
 *
 * @example EntryExitPackageDeliveryWidget();
 *
 * @global $extra The $extra variable contains the options for the Search function
 * @global $_ROSARIO sets $_ROSARIO['SearchTerms']
 */
function EntryExitPackageDeliveryWidget()
{
	global $extra,
		$_ROSARIO;

	// If checkbox checked selected.
	if ( ! empty( $_REQUEST['has_package_to_pickup'] ) )
	{
		// Limit student search to subject.
		$extra['WHERE'] =  " AND exists(SELECT 1
			FROM program_user_config puc
			WHERE puc.TITLE='HAS_PACKAGE_TO_PICKUP'
			AND puc.PROGRAM='EntryExitStudent'
			AND VALUE IS NOT NULL
			AND puc.USER_ID=(s.STUDENT_ID * -1))";

		// Add SearchTerms.
		if ( empty( $extra['NoSearchTerms'] ) )
		{
			$_ROSARIO['SearchTerms'] = issetVal( $_ROSARIO['SearchTerms'], '' );

			$_ROSARIO['SearchTerms'] .= '<b>' . dgettext( 'Entry_Exit', 'Has a package to pickup' ) . '</b><br />';
		}
	}

	// Add Widget to Search.
	$extra['search'] = '<tr><td><label for="has_package_to_pickup">' .
		dgettext( 'Entry_Exit', 'Has a package to pickup' ) . '</label></td><td>' .
		CheckboxInput(
			'',
			'has_package_to_pickup',
			'',
			'',
			true
		) . '</td></tr>';
}

function EntryExitPackageDeliveryHistoryStartEndDateHeader()
{
	return _( 'From' ) . ' ' . PrepareDate( $_REQUEST['start'], '_start', false ) .
		' - ' . _( 'To' ) . ' ' . PrepareDate( $_REQUEST['end'], '_end', true ) . ' ' .
		Buttons( _( 'Go' ) );
}

function EntryExitPackageDeliveryHistorySQL( $start_date, $end_date = '' )
{
	global $extra;

	$records_sql_where = " AND DELIVERED_AT>='" . $start_date . " 00:00:00'";

	if ( $end_date )
	{
		$records_sql_where .= " AND DELIVERED_AT<='" . $end_date . " 23:59:59'";
	}

	if ( ! empty( $_REQUEST['advanced_search'] ) )
	{
		$extra['SELECT_ONLY'] = DBSQLCommaSeparatedResult( 's.STUDENT_ID', ',' ) . " AS STUDENTS_LIST";

		$students_RET = GetStuList( $extra );

		$records_sql_where .= " AND pd.STUDENT_ID IN(" . issetVal( $students_RET[1]['STUDENTS_LIST'], '0' ) . ")";
	}

	$sql = "SELECT pd.ID,pd.STUDENT_ID,
		COMMENTS,DELIVERED_AT,'' AS CHECKBOX,ssm.GRADE_ID,
		" . DisplayNameSQL( 's' ) . " AS FULL_NAME
		FROM entry_exit_package_deliveries pd,students s
		JOIN student_enrollment ssm ON (ssm.STUDENT_ID=s.STUDENT_ID
			AND ssm.ID=(SELECT ID
				FROM student_enrollment
				WHERE STUDENT_ID=ssm.STUDENT_ID
				AND SYEAR='" . UserSyear() . "'
				ORDER BY SYEAR DESC,START_DATE DESC
				LIMIT 1))
		WHERE pd.STUDENT_ID=s.STUDENT_ID
		" . $records_sql_where . "
		ORDER BY DELIVERED_AT DESC,FULL_NAME";

	if ( function_exists( 'SQLLimitForList' ) )
	{
		// @since RosarioSIS 11.7 Limit SQL result for List
		$sql_count = "SELECT COUNT(1)
			FROM entry_exit_package_deliveries pd,students s
			JOIN student_enrollment ssm ON (ssm.STUDENT_ID=s.STUDENT_ID
				AND ssm.ID=(SELECT ID
					FROM student_enrollment
					WHERE STUDENT_ID=ssm.STUDENT_ID
					AND SYEAR='" . UserSyear() . "'
					ORDER BY SYEAR DESC,START_DATE DESC
					LIMIT 1))
			WHERE pd.STUDENT_ID=s.STUDENT_ID
			" . $records_sql_where;

		$sql .= SQLLimitForList( $sql_count );
	}
	else
	{
		$sql .= " LIMIT 1000";
	}

	return $sql;
}

function EntryExitPackageDeliveryHistoryMakeComments( $value, $column = 'COMMENTS' )
{
	if ( isset( $_REQUEST['_ROSARIO_PDF'] )
		|| ! $value )
	{
		return $value;
	}

	// Truncate value to 36 chars.
	$comments = mb_strlen( $value ) <= 36 ?
		$value :
		'<span title="' . AttrEscape( $value ) . '">' . mb_substr( $value, 0, 33 ) . '...</span>';

	return $comments;
}
