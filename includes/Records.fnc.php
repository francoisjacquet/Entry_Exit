<?php
/**
 * Records functions
 *
 * @package Entry and Exit module
 */


function EntryExitRecordsStartEndDateTimeHeader()
{
	return _( 'From' ) . ' ' . PrepareDate( $_REQUEST['start'], '_start', false ) . ' ' .
		TextInput( $_REQUEST['start_time'], 'start_time', '', 'type="time"', false ) .
		' - ' . _( 'To' ) . ' ' . PrepareDate( $_REQUEST['end'], '_end', true ) . ' ' .
		TextInput( $_REQUEST['end_time'], 'end_time', '', 'type="time"', false ) . ' ' .
		Buttons( _( 'Go' ) );
}

function EntryExitRecordsCheckpointHeader()
{
	$checkpoint_select = EntryExitCheckpointSelectInput(
		$_REQUEST['checkpoint_id'],
		'checkpoint_id',
		'',
		_( 'All' ),
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-post-form"' :
			'onchange="ajaxPostForm(this.form, true);"' )
	);

	return '<label>' . dgettext( 'Entry_Exit', 'Checkpoint' ) . ': ' . $checkpoint_select . '</label>';
}

function EntryExitRecordsTypeHeader()
{
	$type_options = [
		'' => dgettext( 'Entry_Exit', 'Entry and Exit' ),
		'in' => dgettext( 'Entry_Exit', 'Entry' ),
		'out' => dgettext( 'Entry_Exit', 'Exit' ),
	];

	// Do NOT limit Types based on what's configured. Type may change over time for the same Checkpoint!
	$type_select = SelectInput(
		$_REQUEST['record_type'],
		'record_type',
		'',
		$type_options,
		false,
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-post-form"' :
			'onchange="ajaxPostForm(this.form, true);"' ),
		false
	);

	return '<label>' . dgettext( 'Entry_Exit', 'Type' ) . ': ' . $type_select . '</label>';
}

function EntryExitRecordsLastRecordHeader()
{
	return CheckboxInput(
		$_REQUEST['last_record'],
		'last_record',
		dgettext( 'Entry_Exit', 'Only show last record' ),
		'',
		true,
		'Yes',
		'No',
		false,
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-post-form"' :
			'onchange="ajaxPostForm(this.form, true);"' )
	);
}

function EntryExitRecordsUnauthorizedHeader()
{
	return CheckboxInput(
		$_REQUEST['unauthorized'],
		'unauthorized',
		dgettext( 'Entry_Exit', 'Unauthorized' ),
		'',
		true,
		'Yes',
		'No',
		false,
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-post-form"' :
			'onchange="ajaxPostForm(this.form, true);"' )
	);
}

function EntryExitRecordsWhereSQL( $type, $start_datetime, $end_datetime = '' )
{
	$records_sql_where = " AND RECORDED_AT>='" . $start_datetime . "'";

	if ( $end_datetime )
	{
		$records_sql_where .= " AND RECORDED_AT<='" . $end_datetime . "'";
	}

	if ( $_REQUEST['checkpoint_id'] )
	{
		$records_sql_where .= " AND CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'";
	}

	if ( $_REQUEST['record_type'] )
	{
		$records_sql_where .= " AND TYPE='" . $_REQUEST['record_type'] . "'";
	}

	if ( $_REQUEST['last_record'] )
	{
		// https://stackoverflow.com/questions/36186602/mysql-select-only-last-record-of-each-foreign-key
		if ( $type === 'student' )
		{
			$records_sql_where .= " AND stur.RECORDED_AT=(SELECT MAX(stur2.RECORDED_AT)
				FROM entry_exit_student_records stur2
				WHERE stur2.STUDENT_ID=stur.STUDENT_ID";
		}
		else
		{
			$records_sql_where .= " AND stur.RECORDED_AT=(SELECT MAX(stur2.RECORDED_AT)
				FROM entry_exit_staff_records stur2
				WHERE stur2.STAFF_ID=stur.STAFF_ID";
		}

		if ( $_REQUEST['checkpoint_id'] )
		{
			$records_sql_where .= " AND stur2.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'";
		}

		if ( $_REQUEST['record_type'] )
		{
			$records_sql_where .= " AND stur2.TYPE='" . $_REQUEST['record_type'] . "'";
		}

		if ( $_REQUEST['unauthorized'] )
		{
			$records_sql_where .= " AND stur2.UNAUTHORIZED='Y'";
		}

		$records_sql_where .= ")";
	}

	if ( $_REQUEST['unauthorized'] )
	{
		$records_sql_where .= " AND UNAUTHORIZED='Y'";
	}

	return $records_sql_where;
}

function EntryExitStudentRecordsSQL( $start_datetime, $end_datetime = '' )
{
	global $extra;

	$records_sql_where = EntryExitRecordsWhereSQL( 'student', $start_datetime, $end_datetime );

	if ( ! empty( $_REQUEST['advanced_search'] ) )
	{
		$extra['SELECT_ONLY'] = DBSQLCommaSeparatedResult( 's.STUDENT_ID', ',' ) . " AS STUDENTS_LIST";

		$students_RET = GetStuList( $extra );

		$records_sql_where .= " AND stur.STUDENT_ID IN(" . issetVal( $students_RET[1]['STUDENTS_LIST'], '0' ) . ")";
	}

	$records_sql_select = '';

	if ( ! $_REQUEST['checkpoint_id'] )
	{
		$records_sql_select .= ",(SELECT TITLE
			FROM entry_exit_checkpoints
			WHERE ID=stur.CHECKPOINT_ID) AS CHECKPOINT";
	}

	$school_sql_where = empty( $_REQUEST['_search_all_schools'] ) && empty( $_REQUEST['advanced_search'] ) ?
		" AND SCHOOL_ID='" . UserSchool() . "'" : '';

	$sql = "SELECT stur.ID,stur.STUDENT_ID,CHECKPOINT_ID,
		TYPE,UNAUTHORIZED,COMMENTS,RECORDED_AT,'' AS CHECKBOX,ssm.GRADE_ID,
		" . DisplayNameSQL( 's' ) . " AS FULL_NAME
		" . $records_sql_select . "
		FROM entry_exit_student_records stur,students s
		JOIN student_enrollment ssm ON (ssm.STUDENT_ID=s.STUDENT_ID
			AND ssm.ID=(SELECT ID
				FROM student_enrollment
				WHERE STUDENT_ID=ssm.STUDENT_ID
				AND SYEAR='" . UserSyear() . "'
				" . $school_sql_where . "
				ORDER BY SYEAR DESC,START_DATE DESC
				LIMIT 1))
		WHERE stur.STUDENT_ID=s.STUDENT_ID
		" . $records_sql_where . "
		ORDER BY RECORDED_AT DESC,FULL_NAME";

	if ( function_exists( 'SQLLimitForList' ) )
	{
		// @since RosarioSIS 11.7 Limit SQL result for List
		$sql_count = "SELECT COUNT(1)
			FROM entry_exit_student_records stur,students s
			JOIN student_enrollment ssm ON (ssm.STUDENT_ID=s.STUDENT_ID
				AND ssm.ID=(SELECT ID
					FROM student_enrollment
					WHERE STUDENT_ID=ssm.STUDENT_ID
					AND SYEAR='" . UserSyear() . "'
					" . $school_sql_where . "
					ORDER BY SYEAR DESC,START_DATE DESC
					LIMIT 1))
			WHERE stur.STUDENT_ID=s.STUDENT_ID
			" . $records_sql_where;

		$sql .= SQLLimitForList( $sql_count );
	}
	else
	{
		$sql .= " LIMIT 1000";
	}

	return $sql;
}

function EntryExitStaffRecordsSQL( $start_datetime, $end_datetime = '' )
{
	global $extra;

	$records_sql_where = EntryExitRecordsWhereSQL( 'staff', $start_datetime, $end_datetime );

	if ( ! empty( $_REQUEST['advanced_search'] ) )
	{
		// Use $extra['SELECT_ONLY'] only available since RosarioSIS 11.4.
		$extra['SELECT_ONLY'] = DBSQLCommaSeparatedResult( 's.STAFF_ID', ',' ) . " AS STAFF_LIST";

		$staff_RET = GetStaffList( $extra );

		if ( isset( $staff_RET[1]['STAFF_LIST'] ) )
		{
			$staff_list = ! empty( $staff_RET[1]['STAFF_LIST'] ) ? $staff_RET[1]['STAFF_LIST'] : '0';
		}
		else
		{
			$staff_in = [];

			foreach ( $staff_RET as $staff )
			{
				$staff_in[] = $staff['STAFF_ID'];
			}

			$staff_list = $staff_in ? implode( ',', array_map( 'intval', $staff_in ) ) : '0';
		}

		$records_sql_where .= " AND stur.STAFF_ID IN(" . $staff_list . ")";
	}

	$records_sql_select = '';

	if ( ! $_REQUEST['checkpoint_id'] )
	{
		$records_sql_select .= ",(SELECT TITLE
			FROM entry_exit_checkpoints
			WHERE ID=stur.CHECKPOINT_ID) AS CHECKPOINT";
	}

	$sql = "SELECT stur.ID,stur.STAFF_ID,CHECKPOINT_ID,
		TYPE,UNAUTHORIZED,COMMENTS,RECORDED_AT,'' AS CHECKBOX,s.PROFILE,s.PROFILE_ID,
		" . DisplayNameSQL( 's' ) . " AS FULL_NAME
		" . $records_sql_select . "
		FROM entry_exit_staff_records stur,staff s
		WHERE stur.STAFF_ID=s.STAFF_ID
		" . $records_sql_where . "
		ORDER BY RECORDED_AT DESC,FULL_NAME";

	if ( function_exists( 'SQLLimitForList' ) )
	{
		// @since RosarioSIS 11.7 Limit SQL result for List
		$sql_count = "SELECT COUNT(1)
			FROM entry_exit_staff_records stur,staff s
			WHERE stur.STAFF_ID=s.STAFF_ID
			" . $records_sql_where;

		$sql .= SQLLimitForList( $sql_count );
	}
	else
	{
		$sql .= " LIMIT 1000";
	}

	return $sql;
}


function EntryExitRecordsMakeUnauthorized( $value, $column = 'UNAUTHORIZED' )
{
	if ( $value )
	{
		if ( isset( $_REQUEST['LO_save'] ) )
		{
			return dgettext( 'Entry_Exit', 'Unauthorized' );
		}

		return button( 'x' );
	}

	return '';
}


function EntryExitRecordsMakeComments( $value, $column = 'COMMENTS' )
{
	if ( isset( $_REQUEST['_ROSARIO_PDF'] )
		|| ! $value )
	{
		return $value;
	}

	// Truncate value to 36 chars.
	$comments = mb_strlen( $value ) <= 36 ?
		$value :
		'<span title="' . AttrEscape( $value ) . '">' . mb_substr( $value, 0, 33 ) . '...</span>';

	return $comments;
}

function EntryExitRecordsMakeType( $value, $column = 'TYPE' )
{
	$type_options = [
		'in' => dgettext( 'Entry_Exit', 'Entry' ),
		'out' => dgettext( 'Entry_Exit', 'Exit' ),
	];

	if ( ! isset( $type_options[ (string) $value ] ) )
	{
		return $value;
	}

	return $type_options[ $value ];
}
