<?php
/**
 * Add Records functions
 *
 * @package Entry and Exit module
 */

function EntryExitAddRecordsPopTableOutput()
{
	PopTable( 'header', dgettext( 'Entry_Exit', 'Add Records' ) );

	echo '<table class="cellpadding-5">';

	echo '<tr class="st"><td colspan="2">' . EntryExitCheckpointSelectInput(
		'',
		'checkpoint_id',
		dgettext( 'Entry_Exit', 'Checkpoint' ),
		'N/A',
		'required'
	) . '</td>';

	$type_options = [
		'in' => dgettext( 'Entry_Exit', 'Entry' ),
		'out' => dgettext( 'Entry_Exit', 'Exit' ),
	];

	echo '<tr class="st"><td colspan="2">' . SelectInput(
		'',
		'record_type',
		dgettext( 'Entry_Exit', 'Type' ),
		$type_options,
		'N/A',
		'required'
	) . '</td></tr>';

	echo '<tr class="st"><td>' . DateInput(
		DBDate(),
		'recorded_at',
		_( 'Date' ),
		false,
		true,
		true
	) . '</td>';

	echo '<td>' . TextInput(
		'',
		'recorded_at_time',
		dgettext( 'Entry_Exit', 'Time' ),
		'required type="time"'
	) . '</td></tr>';

	echo '<tr class="st"><td colspan="2">' . CheckboxInput(
		'',
		'unauthorized',
		dgettext( 'Entry_Exit', 'Unauthorized' ),
		'',
		true
	) . '</td></tr>';

	echo '<tr class="st"><td colspan="2">' . TextInput(
		'',
		'comments',
		_( 'Comments' ),
		'maxlength="100" size="30"'
	) . '</td>';

	echo '</table>';

	PopTable( 'footer' );
}
