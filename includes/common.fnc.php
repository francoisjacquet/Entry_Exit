<?php
/**
 * Common functions
 *
 * @package Entry and Exit module
 */

/**
 * Format Authorized time value for display
 *
 * @deprecated since RosarioSIS 11.5 use ProperTime()
 *
 * @param string $time Time value from DB.
 *
 * @return string Formatted time.
 */
function EntryExitFormatTime( $time, $column = 'RETURN_TIME' )
{
	if ( ! $time )
	{
		return $time;
	}

	// Preferred time based on locale.
	$time = strftime_compat( '%X', strtotime( $time ) );

	// Strip seconds :00.
	$time = mb_substr( $time, -3 ) === ':00' ? mb_substr( $time, 0, -3 ) : $time;

	// Strip seconds when AM/PM :00.
	// 0x202f is hex for "narrow no-break space" unicode character.
	$time = str_replace( [ ':00 ', ':00 ' ], [ ' ', ' ' ], $time );

	return $time;
}


function EntryExitCheckpointSelectInput( $value, $name, $title = '', $allow_na = 'N/A', $extra = '', $div = false )
{
	$checkpoints_RET = DBGet( "SELECT ID,TITLE
	FROM entry_exit_checkpoints
	ORDER BY SORT_ORDER IS NULL,SORT_ORDER,TITLE" );

	$options = [];

	foreach ( $checkpoints_RET as $checkpoint )
	{
		$options[ $checkpoint['ID'] ] = $checkpoint['TITLE'];
	}

	return SelectInput(
		$value,
		$name,
		$title,
		$options,
		$allow_na,
		$extra,
		$div
	);
}

function EntryExitMakeDateTime( $value, $column = 'RECORDED_AT' )
{
	if ( ! $value )
	{
		return $value;
	}

	return ProperDateTime( $value, 'short' );
}

function EntryExitIsUnauthorized( $checkpoint_id, $time )
{
	// Automatically calculate unauthorized status
	// based on checkpoint's "Authorized From" & "Authorized To" times.
	$checkpoint_RET = DBGet( "SELECT AUTHORIZED_FROM,AUTHORIZED_TO
		FROM entry_exit_checkpoints
		WHERE ID='" . (int) $checkpoint_id . "'" );

	$authorized_from = issetVal( $checkpoint_RET[1]['AUTHORIZED_FROM'] );

	$authorized_to = issetVal( $checkpoint_RET[1]['AUTHORIZED_TO'] );

	if ( ! $authorized_from
		|| ! $authorized_to )
	{
		return false;
	}

	// When From time posterior to To time, ie. 8PM to 7AM.
	if ( $authorized_from > $authorized_to
		&& ( $time >= $authorized_from
			|| $time <= $authorized_to ) )
	{
		return false;
	}

	// When From time anterior to To time, ie. 7AM to 8PM.
	return $time < $authorized_from || $time > $authorized_to;
}

function EntryExitIsStudentUnauthorized( $student_id, $checkpoint_id, $date, $time )
{
	global $DatabaseType;

	if ( ! EntryExitIsUnauthorized( $checkpoint_id, $time ) )
	{
		// Student is authorized.
		return false;
	}

	// Student is unauthorized based on checkpoint's "Authorized From" & "Authorized To" times.
	// Check if student has evening leave for date.
	$return_time = DBGetOne( "SELECT RETURN_TIME
		FROM entry_exit_evening_leaves eeel
		WHERE eeel.STUDENT_ID='" . (int) $student_id . "'
		AND eeel.CHECKPOINT_ID='" . (int) $checkpoint_id . "'
		AND eeel.SYEAR='" . UserSyear() . "'
		AND '" . $date . "' BETWEEN eeel.FROM_DATE AND eeel.TO_DATE
		AND position(substring('UMTWHFS' FROM " . ( $DatabaseType === 'mysql' ?
			"DAYOFWEEK('" . $date . "')" :
			"cast(extract(DOW FROM cast('" . $date . "' AS date))+1 AS int)" ) .
		" FOR 1) IN eeel.DAYS_OF_WEEK)>0
		LIMIT 1" );

	if ( ! $return_time )
	{
		// No evening leave, unauthorized.
		return true;
	}

	// Has evening leave for this date and checkpoint.
	// Compare return time with time.
	return $time > $return_time;
}

function EntryExitGetLastStaffRecordType( $checkpoint_id, $staff_id )
{
	return (string) DBGetOne( "SELECT TYPE
		FROM entry_exit_staff_records
		WHERE STAFF_ID='" . (int) $staff_id . "'
		AND CHECKPOINT_ID='" . (int) $checkpoint_id . "'
		ORDER BY RECORDED_AT DESC
		LIMIT 1" );
}

function EntryExitGetLastStudentRecordType( $checkpoint_id, $student_id )
{
	return (string) DBGetOne( "SELECT TYPE
		FROM entry_exit_student_records
		WHERE STUDENT_ID='" . (int) $student_id . "'
		AND CHECKPOINT_ID='" . (int) $checkpoint_id . "'
		ORDER BY RECORDED_AT DESC
		LIMIT 1" );
}
