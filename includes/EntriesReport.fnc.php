<?php
/**
 * Entries Report functions
 *
 * @package Entry and Exit module
 */

function EntryExitEntriesReportCheckpointHeader()
{
	$checkpoint_select = EntryExitCheckpointSelectInput(
		$_REQUEST['checkpoint_id'],
		'checkpoint_id',
		'',
		false,
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-post-form"' :
			'onchange="ajaxPostForm(this.form, true);"' )
	);

	return '<label>' . dgettext( 'Entry_Exit', 'Checkpoint' ) . ': ' . $checkpoint_select . '</label>';
}

function EntryExitEntriesReportDateHeader()
{
	return PrepareDate( $_REQUEST['return'], '_return', false, [ 'submit' => true ] );
}

function EntryExitEntriesReportSearchExtra()
{
	global $extra,
		$DatabaseType;

	if ( empty( $_REQUEST['checkpoint_id'] )
		|| empty( $_REQUEST['return'] ) )
	{
		return '';
	}

	$extra['new'] = true;
	$extra['link']['FULL_NAME'] = false;

	$extra['functions']['FULL_NAME'] = 'makePhotoTipMessage';

	$extra['SELECT'] = issetVal( $extra['SELECT'], '' );

	// Get "Attendance Teacher".
	$extra['SELECT'] .= ",(SELECT " . DisplayNameSQL( 'st' ) . "
	FROM staff st,course_periods cp,school_periods p,schedule ss,course_period_school_periods cpsp
	WHERE st.STAFF_ID=cp.TEACHER_ID
	AND cpsp.PERIOD_id=p.PERIOD_ID
	AND cp.DOES_ATTENDANCE IS NOT NULL
	AND cpsp.COURSE_PERIOD_ID=cp.COURSE_PERIOD_ID
	AND cp.COURSE_PERIOD_ID=ss.COURSE_PERIOD_ID
	AND ss.STUDENT_ID=s.STUDENT_ID
	AND ss.SYEAR='" . UserSyear() . "'
	AND ss.MARKING_PERIOD_ID IN (" . GetAllMP( 'QTR', GetCurrentMP( 'QTR', DBDate(), false ) ) . ")
	AND (ss.START_DATE<='" . DBDate() . "'
		AND (ss.END_DATE>='" . DBDate() . "' OR ss.END_DATE IS NULL))
	ORDER BY p.SORT_ORDER IS NULL,p.SORT_ORDER LIMIT 1) AS ATTENDANCE_TEACHER";

	$extra['columns_after']['ATTENDANCE_TEACHER'] = _( 'Attendance Teacher' );

	// Select Evening Leave Return Time if any for date.
	$extra['SELECT'] .= ",(SELECT RETURN_TIME
		FROM entry_exit_evening_leaves eeel
		WHERE eeel.STUDENT_ID=s.STUDENT_ID
		AND eeel.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'
		AND eeel.SYEAR='" . UserSyear() . "'
		AND '" . $_REQUEST['return'] . "' BETWEEN eeel.FROM_DATE AND eeel.TO_DATE
		AND position(substring('UMTWHFS' FROM " . ( $DatabaseType === 'mysql' ?
			"DAYOFWEEK('" . $_REQUEST['return'] . "')" :
			"cast(extract(DOW FROM cast('" . $_REQUEST['return'] . "' AS date))+1 AS int)" ) .
		" FOR 1) IN eeel.DAYS_OF_WEEK)>0
		LIMIT 1) AS EVENING_LEAVE";

	$extra['columns_after']['EVENING_LEAVE'] = dgettext( 'Entry_Exit', 'Evening Leave' );

	$extra['functions']['EVENING_LEAVE'] = 'EntryExitFormatTime';

	// Select Evening Leave comments if any.
	$extra['SELECT'] .= ",(SELECT COMMENTS
		FROM entry_exit_evening_leaves eeel
		WHERE eeel.STUDENT_ID=s.STUDENT_ID
		AND eeel.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'
		AND eeel.SYEAR='" . UserSyear() . "'
		AND '" . $_REQUEST['return'] . "' BETWEEN eeel.FROM_DATE AND eeel.TO_DATE
		AND position(substring('UMTWHFS' FROM " . ( $DatabaseType === 'mysql' ?
			"DAYOFWEEK('" . $_REQUEST['return'] . "')" :
			"cast(extract(DOW FROM cast('" . $_REQUEST['return'] . "' AS date))+1 AS int)" ) .
		" FOR 1) IN eeel.DAYS_OF_WEEK)>0
		LIMIT 1) AS EVENING_LEAVE_COMMENTS";

	$extra['columns_after']['EVENING_LEAVE_COMMENTS'] = _( 'Comments' );

	$extra['functions']['EVENING_LEAVE_COMMENTS'] = 'EntryExitEntriesReportMakeEveningLeaveComments';

	// Get "Return Time", that is last Record of the selected type for this checkpoint, today.
	$extra['SELECT'] .= ",(SELECT eesr.RECORDED_AT
		FROM entry_exit_student_records eesr
		WHERE eesr.STUDENT_ID=s.STUDENT_ID
		AND eesr.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'
		AND eesr.TYPE='in'
		AND eesr.RECORDED_AT BETWEEN '" . $_REQUEST['return'] . " 00:00:00'
			AND '" . $_REQUEST['return'] . " 23:59:59'
		ORDER BY eesr.RECORDED_AT DESC
		LIMIT 1) AS RETURN_TIME";

	// Get "Return Time" comments.
	$extra['SELECT'] .= ",(SELECT eesr.COMMENTS
		FROM entry_exit_student_records eesr
		WHERE eesr.STUDENT_ID=s.STUDENT_ID
		AND eesr.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'
		AND eesr.TYPE='in'
		AND eesr.RECORDED_AT BETWEEN '" . $_REQUEST['return'] . " 00:00:00'
			AND '" . $_REQUEST['return'] . " 23:59:59'
		ORDER BY eesr.RECORDED_AT DESC
		LIMIT 1) AS RETURN_TIME_COMMENTS";

	$extra['columns_after']['RETURN_TIME'] = dgettext( 'Entry_Exit', 'Entry' );

	$extra['functions']['RETURN_TIME'] = 'EntryExitEntriesReportMakeReturnTime';

	$extra['WHERE'] = issetVal( $extra['WHERE'], '' );

	// Do not ORDER BY ALIAS IS NULL here PostgreSQL generates an error...
	$extra['ORDER_BY'] = "EVENING_LEAVE,
		RETURN_TIME,
		FULL_NAME";

	return $extra;
}

// "Return Time" record if any.
// Add Delay if any (+X minutes)
// Add Comments inside TipMessage.
function EntryExitEntriesReportMakeReturnTime( $value, $column = 'RETURN_TIME' )
{
	global $THIS_RET;

	if ( ! $value )
	{
		return '';
	}

	$time = EntryExitFormatTime( $value );

	// Add Delay if any.
	if ( ! empty( $THIS_RET['EVENING_LEAVE'] ) )
	{
		$return_datetime = $value;

		$evening_leave_datetime = $_REQUEST['return'] . ' ' . $THIS_RET['EVENING_LEAVE'];

		if ( $return_datetime > $evening_leave_datetime )
		{
			// We have a delay, calculate minutes.
			$return_seconds = strtotime( $return_datetime );

			$evening_leave_seconds = strtotime( $evening_leave_datetime );

			$delay_minutes = round( ( $return_seconds - $evening_leave_seconds ) / 60 );

			if ( $delay_minutes )
			{
				$time .= ' <span class="size-3">' . sprintf(
					dgettext( 'Entry_Exit', '+%d minutes' ),
					$delay_minutes
				) . '</span>';
			}
		}
	}

	if ( empty( $THIS_RET['RETURN_TIME_COMMENTS'] ) )
	{
		return $time;
	}

	return MakeTipMessage(
		$THIS_RET['RETURN_TIME_COMMENTS'],
		_( 'Comments' ),
		$time
	);
}

function EntryExitEntriesReportMakeEveningLeaveComments( $value, $column = 'EVENING_LEAVE_COMMENTS' )
{
	if ( isset( $_REQUEST['_ROSARIO_PDF'] )
		|| ! $value )
	{
		return $value;
	}

	// Truncate value to 36 chars.
	$notes = mb_strlen( $value ) <= 36 ?
		$value :
		'<span title="' . AttrEscape( $value ) . '">' . mb_substr( $value, 0, 33 ) . '...</span>';

	return $notes;
}
