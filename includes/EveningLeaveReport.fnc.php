<?php
/**
 * Evening Leave report functions
 *
 * @package Entry and Exit module
 */

function EntryExitEveningLeaveCheckpointHeader()
{
	$checkpoint_select = EntryExitCheckpointSelectInput(
		$_REQUEST['checkpoint_id'],
		'checkpoint_id',
		'',
		false,
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-post-form"' :
			'onchange="ajaxPostForm(this.form, true);"' )
	);

	return '<label>' . dgettext( 'Entry_Exit', 'Checkpoint' ) . ': ' . $checkpoint_select . '</label>';
}

function EntryExitEveningLeaveDateHeader()
{
	return PrepareDate( $_REQUEST['return'], '_return', false, [ 'submit' => true ] );
}

function EntryExitEveningLeaveTypeHeader()
{
	$type_options = [
		'in' => dgettext( 'Entry_Exit', 'Entry' ),
		'out' => dgettext( 'Entry_Exit', 'Exit' ),
	];

	// Do NOT limit Types based on what's configured. Type may change over time for the same Checkpoint!
	$type_select = SelectInput(
		$_REQUEST['record_type'],
		'record_type',
		'',
		$type_options,
		false,
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-post-form"' :
			'onchange="ajaxPostForm(this.form, true);"' ),
		false
	);

	return '<label>' . dgettext( 'Entry_Exit', 'Type' ) . ': ' . $type_select . '</label>';
}

function EntryExitEveningLeaveReturnTimeHeader()
{
	$time_input = TextInput(
		mb_substr( $_REQUEST['return_time'], 0, 5 ),
		'return_time',
		'',
		'type="time"',
		false
	);

	return '<label>' . dgettext( 'Entry_Exit', 'Authorized Return Time' ) . ': ' . $time_input . '</label>';
}

function EntryExitEveningLeaveHideHeader()
{
	return CheckboxInput(
		$_REQUEST['hide_students_without_evening_leave'],
		'hide_students_without_evening_leave',
		dgettext( 'Entry_Exit', 'Hide students without evening leave' ),
		'',
		true,
		'Yes',
		'No',
		false,
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-post-form"' :
			'onchange="ajaxPostForm(this.form, true);"' )
	);
}

/**
 * Status legend
 * 1. Empty if now is before "Evening Leave" / "Authorized Return Time" and no entries recorded yet.
 * 2. Green tick Authorized if entry recorded before "Evening Leave" / "Authorized Return Time".
 * 3. Warning if now is after "Evening Leave" / "Authorized Return Time" and no entries recorded yet.
 * 4. Red cross Unauthorized if entry recorded after "Evening Leave" / "Authorized Return Time".
 *
 * @param string $value  Empty.
 * @param string $column Column name.
 *
 * @return string Status icon.
 */
function EntryExitEveningLeaveStatusLegendHeader()
{
	$status_button = [
		'' => '',
		'authorized' => button( 'check' ),
		'warning' => button( 'warning' ),
		'unauthorized' => button( 'x' ),
	];

	$status_legend = [
		'' => dgettext( 'Entry_Exit', 'Empty if now is before "Evening Leave" / "Authorized Return Time" and no entries recorded yet.' ),
		'authorized' => dgettext( 'Entry_Exit', 'Authorized if entry recorded before "Evening Leave" / "Authorized Return Time".' ),
		'warning' => dgettext( 'Entry_Exit', 'Warning if now is after "Evening Leave" / "Authorized Return Time" and no entries recorded yet.' ),
		'unauthorized' => dgettext( 'Entry_Exit', 'Unauthorized if entry recorded after "Evening Leave" / "Authorized Return Time".' ),
	];

	$legend_html = '<table class="size-1 cellpadding-5"><tr><td>' . $status_button[''] . '</td>
		<td>' . $status_legend[''] . '</td></tr>';
	$legend_html .= '<tr><td>' . $status_button['authorized'] . '</td>
		<td>' . $status_legend['authorized'] . '</td></tr>';
	$legend_html .= '<tr><td>' . $status_button['warning'] . '</td>
		<td>' . $status_legend['warning'] . '</td></tr>';
	$legend_html .= '<tr><td>' . $status_button['unauthorized'] . '</td>
		<td>' . $status_legend['unauthorized'] . '</td></tr></table>';

	return $legend_html .
		FormatInputTitle( _( 'Status' ), '', false, '' );
}

function EntryExitEveningLeaveSearchExtra()
{
	global $extra,
		$DatabaseType;

	if ( empty( $_REQUEST['checkpoint_id'] )
		|| empty( $_REQUEST['return'] )
		|| empty( $_REQUEST['return_time'] ) )
	{
		return '';
	}

	$extra['new'] = true;
	$extra['link']['FULL_NAME'] = false;

	$extra['functions']['FULL_NAME'] = 'makePhotoTipMessage';

	$extra['SELECT'] = issetVal( $extra['SELECT'], '' );

	// Select Evening Leave Return Time if any for date.
	$extra['SELECT'] .= ",(SELECT RETURN_TIME
		FROM entry_exit_evening_leaves eeel
		WHERE eeel.STUDENT_ID=s.STUDENT_ID
		AND eeel.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'
		AND eeel.SYEAR='" . UserSyear() . "'
		AND '" . $_REQUEST['return'] . "' BETWEEN eeel.FROM_DATE AND eeel.TO_DATE
		AND position(substring('UMTWHFS' FROM " . ( $DatabaseType === 'mysql' ?
			"DAYOFWEEK('" . $_REQUEST['return'] . "')" :
			"cast(extract(DOW FROM cast('" . $_REQUEST['return'] . "' AS date))+1 AS int)" ) .
		" FOR 1) IN eeel.DAYS_OF_WEEK)>0
		LIMIT 1) AS EVENING_LEAVE";

	// Select Evening Leave comments if any.
	$extra['SELECT'] .= ",(SELECT COMMENTS
		FROM entry_exit_evening_leaves eeel
		WHERE eeel.STUDENT_ID=s.STUDENT_ID
		AND eeel.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'
		AND eeel.SYEAR='" . UserSyear() . "'
		AND '" . $_REQUEST['return'] . "' BETWEEN eeel.FROM_DATE AND eeel.TO_DATE
		AND position(substring('UMTWHFS' FROM " . ( $DatabaseType === 'mysql' ?
			"DAYOFWEEK('" . $_REQUEST['return'] . "')" :
			"cast(extract(DOW FROM cast('" . $_REQUEST['return'] . "' AS date))+1 AS int)" ) .
		" FOR 1) IN eeel.DAYS_OF_WEEK)>0
		LIMIT 1) AS EVENING_LEAVE_COMMENTS";

	// Select Evening Leave created by if any.
	$extra['SELECT'] .= ",(SELECT CREATED_BY
		FROM entry_exit_evening_leaves eeel
		WHERE eeel.STUDENT_ID=s.STUDENT_ID
		AND eeel.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'
		AND eeel.SYEAR='" . UserSyear() . "'
		AND '" . $_REQUEST['return'] . "' BETWEEN eeel.FROM_DATE AND eeel.TO_DATE
		AND position(substring('UMTWHFS' FROM " . ( $DatabaseType === 'mysql' ?
			"DAYOFWEEK('" . $_REQUEST['return'] . "')" :
			"cast(extract(DOW FROM cast('" . $_REQUEST['return'] . "' AS date))+1 AS int)" ) .
		" FOR 1) IN eeel.DAYS_OF_WEEK)>0
		LIMIT 1) AS EVENING_LEAVE_CREATED_BY";

	$extra['columns_after']['EVENING_LEAVE'] = dgettext( 'Entry_Exit', 'Evening Leave' );

	$extra['functions']['EVENING_LEAVE'] = 'EntryExitEveningLeaveMakeEveningLeave';

	// Get "Return Time", that is last Record of the selected type for this checkpoint, today.
	$extra['SELECT'] .= ",(SELECT eesr.RECORDED_AT
		FROM entry_exit_student_records eesr
		WHERE eesr.STUDENT_ID=s.STUDENT_ID
		AND eesr.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'
		AND eesr.TYPE='" . $_REQUEST['record_type'] . "'
		AND eesr.RECORDED_AT BETWEEN '" . $_REQUEST['return'] . " 00:00:00'
			AND '" . $_REQUEST['return'] . " 23:59:59'
		ORDER BY eesr.RECORDED_AT DESC
		LIMIT 1) AS RETURN_TIME";

	// Get "Return Time" comments.
	$extra['SELECT'] .= ",(SELECT eesr.COMMENTS
		FROM entry_exit_student_records eesr
		WHERE eesr.STUDENT_ID=s.STUDENT_ID
		AND eesr.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'
		AND eesr.TYPE='" . $_REQUEST['record_type'] . "'
		AND eesr.RECORDED_AT BETWEEN '" . $_REQUEST['return'] . " 00:00:00'
			AND '" . $_REQUEST['return'] . " 23:59:59'
		ORDER BY eesr.RECORDED_AT DESC
		LIMIT 1) AS RETURN_TIME_COMMENTS";

	$extra['columns_after']['RETURN_TIME'] = dgettext( 'Entry_Exit', 'Return Time' );

	$extra['functions']['RETURN_TIME'] = 'EntryExitEveningLeaveMakeReturnTime';


	$extra['SELECT'] .= ",'' AS STATUS";

	$extra['columns_after']['STATUS'] = _( 'Status' );

	$extra['functions']['STATUS'] = 'EntryExitEveningLeaveMakeStatus';


	$extra['SELECT'] .= ",(SELECT VALUE
		FROM program_user_config
		WHERE s.STUDENT_ID=(USER_ID *-1)
		AND PROGRAM='EntryExitStudent'
		AND TITLE='PRIVATE_NOTES'
		LIMIT 1) AS PRIVATE_NOTES";

	$extra['columns_after']['PRIVATE_NOTES'] = dgettext( 'Entry_Exit', 'Private Notes' );

	$extra['functions']['PRIVATE_NOTES'] = 'EntryExitEveningLeaveMakePrivateNotes';

	$extra['WHERE'] = issetVal( $extra['WHERE'], '' );

	if ( ! empty( $_REQUEST['hide_students_without_evening_leave'] ) )
	{
		// Hide students without evening leave for today.
		$extra['WHERE'] .= " AND (SELECT RETURN_TIME
			FROM entry_exit_evening_leaves eeel
			WHERE eeel.STUDENT_ID=s.STUDENT_ID
			AND eeel.CHECKPOINT_ID='" . (int) $_REQUEST['checkpoint_id'] . "'
			AND eeel.SYEAR='" . UserSyear() . "'
			AND '" . $_REQUEST['return'] . "' BETWEEN eeel.FROM_DATE AND eeel.TO_DATE
			AND position(substring('UMTWHFS' FROM " . ( $DatabaseType === 'mysql' ?
				"DAYOFWEEK('" . $_REQUEST['return'] . "')" :
				"cast(extract(DOW FROM cast('" . $_REQUEST['return'] . "' AS date))+1 AS int)" ) .
			" FOR 1) IN eeel.DAYS_OF_WEEK)>0
			LIMIT 1) IS NOT NULL";
	}

	// Order set in &LO_sort, here PostgreSQL generates an error...
	// $extra['ORDER_BY'] = "EVENING_LEAVE,EVENING_LEAVE IS NULL,FULL_NAME";

	return $extra;
}

// Evening Leave Return Time.
// Add Comments and Created by inside TipMessage.
function EntryExitEveningLeaveMakeEveningLeave( $value, $column = 'EVENING_LEAVE' )
{
	global $THIS_RET;

	if ( ! $value )
	{
		return '';
	}

	$time = EntryExitFormatTime( $value );

	$tipmessage = [];

	if ( ! empty( $THIS_RET['EVENING_LEAVE_COMMENTS'] ) )
	{
		$tipmessage[] = NoInput(
			$THIS_RET['EVENING_LEAVE_COMMENTS'],
			_( 'Comments' )
		);
	}

	if ( ! empty( $THIS_RET['EVENING_LEAVE_CREATED_BY'] ) )
	{
		$tipmessage[] = NoInput(
			$THIS_RET['EVENING_LEAVE_CREATED_BY'],
			_( 'Created by' )
		);
	}

	if ( ! $tipmessage )
	{
		return $time;
	}

	return MakeTipMessage(
		implode( '<br>', $tipmessage ),
		dgettext( 'Entry_Exit', 'Evening Leave' ),
		$time
	);
}

// "Return Time" record if any.
// Add Comments inside TipMessage.
function EntryExitEveningLeaveMakeReturnTime( $value, $column = 'RETURN_TIME' )
{
	global $THIS_RET;

	if ( ! $value )
	{
		return '';
	}

	$time = EntryExitFormatTime( $value );

	if ( empty( $THIS_RET['RETURN_TIME_COMMENTS'] ) )
	{
		return $time;
	}

	return MakeTipMessage(
		$THIS_RET['RETURN_TIME_COMMENTS'],
		_( 'Comments' ),
		$time
	);
}


/**
 * Status
 * 1. Empty if now is before "Evening Leave" / "Authorized Return Time" and no entries recorded yet.
 * 2. Green tick Authorized if entry recorded before "Evening Leave" / "Authorized Return Time".
 * 3. Warning if now is after "Evening Leave" / "Authorized Return Time" and no entries recorded yet.
 * 4. Red cross Unauthorized if entry recorded after "Evening Leave" / "Authorized Return Time".
 *
 * @param string $value  Empty.
 * @param string $column Column name.
 *
 * @return string Status icon.
 */
function EntryExitEveningLeaveMakeStatus( $value, $column = 'STATUS' )
{
	global $THIS_RET;

	$status_button = [
		'authorized' => '<!-- authorized -->' . button( 'check' ),
		'warning' => '<!-- warning -->' . button( 'warning' ),
		'unauthorized' => '<!-- unauthorized -->' . button( 'x' ),
	];

	$status_button_text = [
		'authorized' => button( 'check', dgettext( 'Entry_Exit', 'Unauthorized' ) ),
		'warning' => button( 'warning', _( 'Warning' ) ),
		'unauthorized' => button( 'x', dgettext( 'Entry_Exit', 'Authorized' ) ),
	];

	$now_time = date( 'H:i:s' );

	$today = DBDate();

	$authorized_return_time = $_REQUEST['return_time'];

	if ( ! empty( $THIS_RET['EVENING_LEAVE'] )
		&& $THIS_RET['EVENING_LEAVE'] > $authorized_return_time )
	{
		// Student has evening leave.
		$authorized_return_time = $THIS_RET['EVENING_LEAVE'];
	}

	if ( empty( $THIS_RET['RETURN_TIME'] ) )
	{
		if ( $today < $_REQUEST['return']
			|| ( $today === $_REQUEST['return'] && $now_time < $authorized_return_time ) )
		{
			// 1. Empty if now is before "Evening Leave" / "Authorized Return Time" and no entries recorded yet.
			return '';
		}

		// 3. Warning if now is after "Evening Leave" / "Authorized Return Time" and no entries recorded yet.
		return isset( $_REQUEST['LO_save'] ) ?
			$status_button_text['warning'] : $status_button['warning'];
	}

	// Remove date from return time (date should be equal to $_REQUEST['return']).
	$return_time = mb_substr( $THIS_RET['RETURN_TIME'], -8 );

	if ( $return_time <= $authorized_return_time )
	{
		// 2. Green tick Authorized if entry recorded before "Evening Leave" / "Authorized Return Time".
		return isset( $_REQUEST['LO_save'] ) ?
			$status_button_text['authorized'] : $status_button['authorized'];
	}

	// 4. Red cross Unauthorized if entry recorded after "Evening Leave" / "Authorized Return Time".
	return isset( $_REQUEST['LO_save'] ) ?
		$status_button_text['unauthorized'] : $status_button['unauthorized'];
}

function EntryExitEveningLeaveMakePrivateNotes( $value, $column = 'PRIVATE_NOTES' )
{
	if ( isset( $_REQUEST['_ROSARIO_PDF'] )
		|| ! $value )
	{
		return $value;
	}

	// Truncate value to 36 chars.
	$notes = mb_strlen( $value ) <= 36 ?
		$value :
		'<span title="' . AttrEscape( $value ) . '">' . mb_substr( $value, 0, 33 ) . '...</span>';

	return $notes;
}
