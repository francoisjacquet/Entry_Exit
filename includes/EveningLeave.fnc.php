<?php
/**
 * Evening Leave functions
 *
 * @package Entry and Exit module
 */

function EntryExitEveningLeaveSeveralDaysListOutput( $student_id )
{
	if ( $student_id < 1 )
	{
		return;
	}

	$envening_leave_sql = EntryExitEveningLeaveSeveralDaysSQL( $student_id );

	$functions = [
		'CHECKPOINT_ID' => 'EntryExitEveningLeaveMakeCheckpoint',
		'FROM_TO_DATES' => 'EntryExitEveningLeaveMakeDates',
		'RETURN_TIME' => 'EntryExitEveningLeaveMakeTime',
		'DAYS_OF_WEEK' => 'EntryExitEveningLeaveMakeDays',
		'COMMENTS' => 'EntryExitEveningLeaveMakeComments',
	];

	$evening_leave_RET = DBGet(
		$envening_leave_sql,
		$functions
	);

	$columns = [
		'CHECKPOINT_ID' => dgettext( 'Entry_Exit', 'Checkpoint' ),
		'FROM_TO_DATES' => _( 'Timeframe' ),
		'DAYS_OF_WEEK' => _( 'Days' ),
		'RETURN_TIME' => dgettext( 'Entry_Exit', 'Return Time' ),
		'COMMENTS' => _( 'Comments' ),
		'CREATED_BY' => _( 'Created by' ),
	];

	$link['add']['html'] = [
		'CHECKPOINT_ID' => EntryExitEveningLeaveMakeCheckpoint( '', 'CHECKPOINT_ID' ),
		'FROM_TO_DATES' => EntryExitEveningLeaveMakeDates( '', 'FROM_TO_DATES' ),
		'RETURN_TIME' => EntryExitEveningLeaveMakeTime( '', 'RETURN_TIME' ),
		'DAYS_OF_WEEK' => EntryExitEveningLeaveMakeDays( '', 'DAYS_OF_WEEK' ),
		'COMMENTS' => EntryExitEveningLeaveMakeComments( '', 'COMMENTS' ),
	];

	$link['add']['first'] = 10; // Number of rows before add link moves to top.

	$link['remove']['link'] = 'Modules.php?modname=' . $_REQUEST['modname'] .
		'&category_id=' . $_REQUEST['category_id'] . '&modfunc=delete_evening_leave';

	$link['remove']['variables'] = [ 'id' => 'ID' ];

	ListOutput(
		$evening_leave_RET,
		$columns,
		dgettext( 'Entry_Exit', 'Evening Leave (several days)' ),
		dgettext( 'Entry_Exit', 'Evening Leaves (several days)' ),
		$link
	);
}

function EntryExitEveningLeaveSeveralDaysSQL( $student_id )
{
	return "SELECT ID,CHECKPOINT_ID,'' AS FROM_TO_DATES,
		FROM_DATE,TO_DATE,DAYS_OF_WEEK,RETURN_TIME,COMMENTS,
		CREATED_AT,UPDATED_AT,CREATED_BY
		FROM entry_exit_evening_leaves
		WHERE STUDENT_ID='" . (int) $student_id . "'
		AND SYEAR='" . UserSyear() . "'
		AND FROM_DATE<>TO_DATE";
}

function EntryExitEveningLeaveSingleDayListOutput( $student_id )
{
	if ( $student_id < 1 )
	{
		return;
	}

	$envening_leave_sql = EntryExitEveningLeaveSingleDaySQL( $student_id );

	$functions = [
		'CHECKPOINT_ID' => 'EntryExitEveningLeaveMakeCheckpoint',
		'FROM_DATE_SINGLE' => 'EntryExitEveningLeaveMakeDate',
		'RETURN_TIME' => 'EntryExitEveningLeaveMakeTime',
		'COMMENTS' => 'EntryExitEveningLeaveMakeComments',
	];

	$evening_leave_RET = DBGet(
		$envening_leave_sql,
		$functions
	);

	$columns = [
		'CHECKPOINT_ID' => dgettext( 'Entry_Exit', 'Checkpoint' ),
		'FROM_DATE_SINGLE' => _( 'Date' ),
		'RETURN_TIME' => dgettext( 'Entry_Exit', 'Return Time' ),
		'COMMENTS' => _( 'Comments' ),
		'CREATED_BY' => _( 'Created by' ),
	];

	$link['add']['html'] = [
		'CHECKPOINT_ID' => EntryExitEveningLeaveMakeCheckpoint( '', 'CHECKPOINT_ID' ),
		'FROM_DATE_SINGLE' => EntryExitEveningLeaveMakeDate( '', 'FROM_DATE_SINGLE' ),
		'RETURN_TIME' => EntryExitEveningLeaveMakeTime( '', 'RETURN_TIME' ),
		'COMMENTS' => EntryExitEveningLeaveMakeComments( '', 'COMMENTS' ),
	];

	$link['add']['first'] = 10; // Number of rows before add link moves to top.

	$link['remove']['link'] = 'Modules.php?modname=' . $_REQUEST['modname'] .
		'&category_id=' . $_REQUEST['category_id'] . '&modfunc=delete_evening_leave';

	$link['remove']['variables'] = [ 'id' => 'ID' ];

	ListOutput(
		$evening_leave_RET,
		$columns,
		dgettext( 'Entry_Exit', 'Evening Leave (single day)' ),
		dgettext( 'Entry_Exit', 'Evening Leaves (single day)' ),
		$link,
		[],
		[ 'valign-middle' => true ]
	);
}

function EntryExitEveningLeaveSingleDaySQL( $student_id )
{
	return "SELECT ID,CHECKPOINT_ID,FROM_DATE AS FROM_DATE_SINGLE,
		FROM_DATE,TO_DATE,DAYS_OF_WEEK,RETURN_TIME,COMMENTS,
		CREATED_AT,UPDATED_AT,CREATED_BY
		FROM entry_exit_evening_leaves
		WHERE STUDENT_ID='" . (int) $student_id . "'
		AND SYEAR='" . UserSyear() . "'
		AND FROM_DATE=TO_DATE";
}


/**
 * Make Checkpoint Input
 *
 * @global array  $THIS_RET
 *
 * @uses EntryExitCheckpointSelectInput()
 *
 * @param  string $value   Field value.
 * @param  string $column  Field column.
 *
 * @return string          Select Input
 */
function EntryExitEveningLeaveMakeCheckpoint( $value, $column = 'CHECKPOINT_ID' )
{
	global $THIS_RET;

	$id = empty( $THIS_RET['ID'] ) ? 'new' : $THIS_RET['ID'];

	return EntryExitCheckpointSelectInput(
		$value,
		'values[' . $id . '][' . $column . ']',
		'',
		( $id === 'new' ? 'N/A' : false ),
		'',
		( $id !== 'new' )
	);
}


/**
 * Make Date Inputs
 *
 * @global array  $THIS_RET
 *
 * @param  string $value   Field value.
 * @param  string $column  Field column.
 *
 * @return string          Date Inputs
 */
function EntryExitEveningLeaveMakeDates( $value, $column = 'FROM_TO_DATES' )
{
	global $THIS_RET;

	$id = empty( $THIS_RET['ID'] ) ? 'new' : $THIS_RET['ID'];

	return '<div id="divEveningLeaveDates' . $id . '" class="rt2colorBox">' .
		EntryExitEveningLeaveMakeDate( issetVal( $THIS_RET['FROM_DATE'], '' ), 'FROM_DATE' ) .
		( $id === 'new' ? '<br />' : '' ) .
		EntryExitEveningLeaveMakeDate( issetVal( $THIS_RET['TO_DATE'], '' ), 'TO_DATE' ) .
		'</div>';
}

/**
 * Make Date Input
 *
 * @global array  $THIS_RET
 *
 * @param  string $value   Field value.
 * @param  string $column  Field column.
 *
 * @return string          Date Input
 */
function EntryExitEveningLeaveMakeDate( $value, $column = 'FROM_DATE' )
{
	global $THIS_RET;

	$id = empty( $THIS_RET['ID'] ) ? 'new' : $THIS_RET['ID'];

	return DateInput(
		$value,
		'values[' . $id . '][' . $column . ']',
		( $column === 'FROM_DATE' ? _( 'From' ) : ( $column === 'TO_DATE' ? _( 'To' ) : '' ) ),
		true,
		// No N/A value for existing entries.
		( $id !== 'new' ? false : 'N/A' )
	);
}

/**
 * Make Comments Input
 *
 * @global array  $THIS_RET
 *
 * @param  string $value   Field value.
 * @param  string $column  Field column.
 *
 * @return string          Comments Input
 */
function EntryExitEveningLeaveMakeComments( $value, $column )
{
	global $THIS_RET;

	$id = empty( $THIS_RET['ID'] ) ? 'new' : $THIS_RET['ID'];

	return TextInput(
		$value,
		'values[' . $id . '][' . $column . ']',
		'',
		'size="12" maxlength="100"'
	);
}

/**
 * Make Time Input
 *
 * @global array  $THIS_RET
 *
 * @uses EntryExitFormatTime()
 *
 * @param  string $value   Field value.
 * @param  string $column  Field column.
 *
 * @return string          Time Input
 */
function EntryExitEveningLeaveMakeTime( $value, $column = 'RETURN_TIME' )
{
	global $THIS_RET;

	$id = empty( $THIS_RET['ID'] ) ? 'new' : $THIS_RET['ID'];

	if ( $value )
	{
		$value = [
			// Strip seconds :00.
			mb_substr( $value, -3 ) === ':00' ? mb_substr( $value, 0, -3 ) : $value,
			EntryExitFormatTime( $value )
		];
	}

	$required = $id === 'new' ? '' : ' required';

	return TextInput(
		$value,
		'values[' . $id . '][' . $column . ']',
		'',
		'type="time"' . $required
	);
}

/**
 * Make Days Input
 *
 * @global array  $THIS_RET
 *
 * @param  string $value   Field value.
 * @param  string $column  Field column.
 *
 * @return string          Days Input
 */
function EntryExitEveningLeaveMakeDays( $value, $column = 'DAYS_OF_WEEK' )
{
	global $THIS_RET;

	$id = empty( $THIS_RET['ID'] ) ? 'new' : $THIS_RET['ID'];

	$day_titles = [];

	$days = [ 'M', 'T', 'W', 'H', 'F', 'S', 'U' ];

	$days_convert = [
		'U' => _( 'Sunday' ),
		'M' => _( 'Monday' ),
		'T' => _( 'Tuesday' ),
		'W' => _( 'Wednesday' ),
		'H' => _( 'Thursday' ),
		'F' => _( 'Friday' ),
		'S' => _( 'Saturday' ),
	];

	$days_html = '';

	foreach ( (array) $days as $day )
	{
		$val = '';

		if ( ( $id === 'new' && $day != 'S' && $day != 'U' )
			|| ( ! empty( $value )
				&& mb_strpos( $value, $day ) !== false ) )
		{
			$val = 'Y';
		}

		$days_html .= CheckboxInput(
			$val,
			'values[' . $id . '][DAYS_OF_WEEK][' . $day . ']',
			$days_convert[$day],
			'',
			true,
			button( 'check' ),
			button( 'x' )
		) . '<br>';

		if ( $val )
		{
			$day_titles[] = mb_substr( $days_convert[$day], 0, 3 ) . '.';
		}
	}

	if ( $id !== 'new' )
	{
		if ( ! AllowEdit()
			|| isset( $_REQUEST['_ROSARIO_PDF'] ) )
		{
			return implode( ' ', $day_titles );
		}

		return '<div id="divEveningLeaveDays' . $id . '" class="rt2colorBox">' . InputDivOnclick(
			'days' . $id,
			$days_html,
			implode( ' ', $day_titles ),
			''
		) . '</div>';
	}

	return '<div id="divEveningLeaveDays' . $id . '" class="rt2colorBox">' . $days_html . '</div>';
}


/**
 * Evening Leave Widget
 *
 * @example EntryExitEveningLeaveWidget();
 *
 * @global $extra The $extra variable contains the options for the Search function
 * @global $_ROSARIO sets $_ROSARIO['SearchTerms']
 */
function EntryExitEveningLeaveWidget()
{
	global $extra,
		$_ROSARIO;

	if ( ! empty( $_REQUEST['evening_leaves_checkpoint_id'] ) )
	{
		$checkpoint_title = DBGetOne( "SELECT TITLE
			FROM entry_exit_checkpoints
			WHERE ID='" . (int) $_REQUEST['evening_leaves_checkpoint_id'] . "'" );
	}

	// If checkbox checked selected.
	if ( ! empty( $_REQUEST['has_no_evening_leaves'] ) )
	{
		// Limit student search.
		$extra['WHERE'] = " AND NOT exists(SELECT 1
			FROM entry_exit_evening_leaves el
			WHERE el.SYEAR='" . UserSyear() . "'
			AND el.STUDENT_ID=s.STUDENT_ID";

		if ( ! empty( $_REQUEST['evening_leaves_checkpoint_id'] ) )
		{
			$extra['WHERE'] .= " AND el.CHECKPOINT_ID='" . (int) $_REQUEST['evening_leaves_checkpoint_id'] . "'";
		}

		$extra['WHERE'] .= ")";

		// Add SearchTerms.
		if ( empty( $extra['NoSearchTerms'] ) )
		{
			$_ROSARIO['SearchTerms'] = issetVal( $_ROSARIO['SearchTerms'], '' );

			$_ROSARIO['SearchTerms'] .= '<b>' . dgettext( 'Entry_Exit', 'Has Evening Leaves' ) . '</b> ' . _( 'No' );

			if ( ! empty( $_REQUEST['evening_leaves_checkpoint_id'] ) )
			{
				$_ROSARIO['SearchTerms'] .= ' &mdash; ' . dgettext( 'Entry_Exit', 'Checkpoint' ) . ': ' . $checkpoint_title;
			}

			$_ROSARIO['SearchTerms'] .= '<br />';
		}
	}
	elseif ( ! empty( $_REQUEST['evening_leaves_checkpoint_id'] ) )
	{
		// Limit student search.
		$extra['WHERE'] = " AND exists(SELECT 1
			FROM entry_exit_evening_leaves el
			WHERE el.SYEAR='" . UserSyear() . "'
			AND el.STUDENT_ID=s.STUDENT_ID
			AND el.CHECKPOINT_ID='" . (int) $_REQUEST['evening_leaves_checkpoint_id'] . "')";

		// Add SearchTerms.
		if ( empty( $extra['NoSearchTerms'] ) )
		{
			$_ROSARIO['SearchTerms'] = issetVal( $_ROSARIO['SearchTerms'], '' );

			$_ROSARIO['SearchTerms'] .= '<b>' . dgettext( 'Entry_Exit', 'Has Evening Leaves' ) . '</b>' .
				' &mdash; ' . dgettext( 'Entry_Exit', 'Checkpoint' ) . ': ' . $checkpoint_title . '<br />';
		}
	}

	// Add Widget to Search.
	$extra['search'] = '<tr><td><label for="has_no_evening_leaves">' .
		dgettext( 'Entry_Exit', 'Has Evening Leaves' ) . '</label></td><td>' .
		CheckboxInput(
			'',
			'has_no_evening_leaves',
			_( 'No' ),
			'',
			true
		) . '<br>' .
		EntryExitCheckpointSelectInput(
			'',
			'evening_leaves_checkpoint_id',
			dgettext( 'Entry_Exit', 'Checkpoint' ),
			'N/A'
		) . '</td></tr>';
}

function EntryExitEveningLeavePopTableOutput()
{
	PopTable( 'header', dgettext( 'Entry_Exit', 'Evening Leave' ) );

	echo '<table class="cellpadding-5">';

	echo '<tr class="st"><td>' . CheckboxInput(
		'',
		'delete_existing_evening_leave',
		dgettext( 'Entry_Exit', 'Delete existing evening leaves for this checkpoint' ),
		'',
		true
	) . '</td></tr>';

	echo '<tr class="st"><td>' . EntryExitCheckpointSelectInput(
		issetVal( $_REQUEST['evening_leaves_checkpoint_id'] ),
		'values[CHECKPOINT_ID]',
		dgettext( 'Entry_Exit', 'Checkpoint' ),
		'N/A',
		'required'
	) . '</td></tr>';

	echo '<tr class="st"><td>' . DateInput(
		DBDate(),
		'values[FROM_DATE]',
		_( 'From' ),
		false,
		true,
		true
	) . '</td></tr>';

	echo '<tr class="st"><td>' . DateInput(
		'',
		'values[TO_DATE]',
		_( 'To' ),
		false,
		true,
		true
	) . '</td></tr>';

	$day_titles = [];

	$days = [ 'M', 'T', 'W', 'H', 'F', 'S', 'U' ];

	$days_convert = [
		'U' => _( 'Sunday' ),
		'M' => _( 'Monday' ),
		'T' => _( 'Tuesday' ),
		'W' => _( 'Wednesday' ),
		'H' => _( 'Thursday' ),
		'F' => _( 'Friday' ),
		'S' => _( 'Saturday' ),
	];

	$days_html = [];

	foreach ( (array) $days as $day )
	{
		$val = '';

		if ( $day != 'S' && $day != 'U' )
		{
			$val = 'Y';
		}

		$days_html[] = CheckboxInput(
			$val,
			'values[DAYS_OF_WEEK][' . $day . ']',
			$days_convert[$day],
			'',
			true
		);
	}

	echo '<tr class="st"><td>' . NoInput(
		implode( '<br>', $days_html ),
		_( 'Days' )
	) . '</td></tr>';

	echo '<tr class="st"><td>' . TextInput(
		'',
		'values[RETURN_TIME]',
		dgettext( 'Entry_Exit', 'Return Time' ),
		'required type="time"'
	) . '</td></tr>';

	echo '<tr class="st"><td colspan="2">' . TextInput(
		'',
		'values[COMMENTS]',
		_( 'Comments' ),
		'maxlength="100" size="30"'
	) . '</td></tr>';

	echo '</table>';

	PopTable( 'footer' );
}
