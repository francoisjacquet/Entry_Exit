/**
 * Update 4.0 SQL (PostgreSQL and MySQL)
 * Add created_by column to entry_exit_evening_leaves table
 *
 * @package Entry and Exit module
 */

--
-- Add created_by column to entry_exit_evening_leaves table
--

ALTER TABLE entry_exit_evening_leaves
ADD created_by text;
