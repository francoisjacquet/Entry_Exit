<?php
/**
 * Add Records (Students)
 *
 * URL to
 * - `&modfunc=save` = Save
 * - `&checkpoint_id=1` = Checkpoint ID 1
 * - `&record_type=in` = Record Type (entry: `in`, exit: `out`)
 * - `&unauthorized=auto` = Unauthorized (authorized: [empty], unauthorized: `Y`, automatic: `auto`)
 * - `&type=student` = Type (students: `student`, users: `staff`)
 * - `&st[]=__STUDENT_ID__` = Student ID
 * - now
 * @example Modules.php?modname=Entry_Exit/AddRecords.php&modfunc=save&checkpoint_id=1&record_type=in&unauthorized=auto&type=student&st[]=__STUDENT_ID__
 *
 * @package Entry and Exit module
 */

if ( $_REQUEST['modfunc'] === 'save' )
{
	$recorded_at = RequestedDate( 'recorded_at', '' );

	if ( empty( $recorded_at ) )
	{
		$recorded_at = DBDate();

		$_REQUEST['recorded_at_time'] = date( 'H:i' );
	}

	if ( empty( $_REQUEST['recorded_at_time'] )
		|| strlen( $_REQUEST['recorded_at_time'] ) < 5 // 12:56
		|| empty( $_REQUEST['checkpoint_id'] ) )
	{
		$error[] = _( 'Please fill in the required fields' );
	}

	$recorded_at .= ' ' . $_REQUEST['recorded_at_time'];

	if ( empty( $_REQUEST['record_type'] ) )
	{
		$_REQUEST['record_type'] = DBGetOne( "SELECT TYPE
			FROM entry_exit_checkpoints
			WHERE ID='" . (int) $_REQUEST['checkpoint_id'] . "'" );

		if ( empty( $_REQUEST['record_type'] ) )
		{
			$last_record_type = EntryExitGetLastStudentRecordType( $checkpoint_id, $student_id );

			$_REQUEST['record_type'] = $last_record_type === 'in' ? 'out' : 'in';
		}
	}

	if ( empty( $_REQUEST['st'] ) )
	{
		$error[] = _( 'You must choose at least one student.' );
	}

	if ( ! $error )
	{
		foreach ( (array) $_REQUEST['st'] as $student_id )
		{
			if ( (int) $student_id < 1 )
			{
				continue;
			}

			$unauthorized = $_REQUEST['unauthorized'];

			if ( $unauthorized === 'auto' )
			{
				$unauthorized = EntryExitIsStudentUnauthorized(
					$student_id,
					$_REQUEST['checkpoint_id'],
					mb_substr( $recorded_at, 0, 10 ),
					$_REQUEST['recorded_at_time']
				);

				$unauthorized = $unauthorized ? 'Y' : '';
			}

			$inserted = DBInsert(
				'entry_exit_student_records',
				[
					'STUDENT_ID' => (int) $student_id,
					'CHECKPOINT_ID' => (int) $_REQUEST['checkpoint_id'],
					'TYPE' => $_REQUEST['record_type'],
					'UNAUTHORIZED' => $unauthorized,
					'COMMENTS' => issetVal( $_REQUEST['comments'], '' ),
					'RECORDED_AT' => $recorded_at,
				]
			);
		}

		if ( ! empty( $inserted ) )
		{
			$note[] = button( 'check' ) . '&nbsp;' . dgettext( 'Entry_Exit', 'Records were added for the selected students.' );
		}
	}

	// Unset modfunc, st & redirect URL.
	RedirectURL( [ 'modfunc', 'st' ] );
}

echo ErrorMessage( $note, 'note' );

echo ErrorMessage( $error );

if ( ! $_REQUEST['modfunc'] )
{
	$extra['link'] = [ 'FULL_NAME' => false ];
	$extra['SELECT'] = ",NULL AS CHECKBOX";

	if ( $_REQUEST['search_modfunc'] === 'list' )
	{
		echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=save' ) . '" method="POST">';

		DrawHeader( '', SubmitButton( dgettext( 'Entry_Exit', 'Add Records to Selected Students' ) ) );

		echo '<br />';

		EntryExitAddRecordsPopTableOutput();

		echo '<br />';
	}

	$extra['functions'] = [ 'CHECKBOX' => 'MakeChooseCheckbox' ];

	$extra['columns_before'] = [
		'CHECKBOX' => MakeChooseCheckbox( 'required', 'STUDENT_ID', 'st' ),
	];

	$extra['new'] = true;

	Search( 'student_id', $extra );

	if ( $_REQUEST['search_modfunc'] === 'list' )
	{
		echo '<br /><div class="center">' .
			SubmitButton( dgettext( 'Entry_Exit', 'Add Records to Selected Students' ) ) . '</div></form>';
	}
}
