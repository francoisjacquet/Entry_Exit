<?php
/**
 * Records Report (Students)
 *
 * @package Entry and Exit module
 */

if ( AllowEdit()
	&& $_REQUEST['modfunc'] === 'delete' )
{
	// Prompt before deleting.
	if ( DeletePrompt( dgettext( 'Entry_Exit', 'Records' ) ) )
	{
		if ( ! empty( $_REQUEST['rec'] ) )
		{
			$records_list = implode( ',', array_map( 'intval', $_REQUEST['rec'] ) );

			DBQuery( "DELETE FROM entry_exit_student_records
				WHERE ID IN(" . $records_list . ")" );
		}

		// Unset modfunc & redirect URL.
		RedirectURL( [ 'modfunc', 'rec' ] );
	}
}

// Defaults to today.
$_REQUEST['start'] = RequestedDate( 'start', DBDate() );

$_REQUEST['start_time'] = ! empty( $_REQUEST['start_time'] ) ? $_REQUEST['start_time'] : '00:00';

$start_datetime = $_REQUEST['start'] . ' ' . $_REQUEST['start_time'];

// Defaults to none.
$end_datetime = '';

$_REQUEST['end'] = RequestedDate( 'end', '' );

$_REQUEST['end_time'] = ! empty( $_REQUEST['end_time'] ) ?
	$_REQUEST['end_time'] :
	( empty( $end_date ) ? '' : '23:59' );

$end_datetime = '';

if ( $_REQUEST['end'] )
{
	$end_datetime = $_REQUEST['end'] . ' ' . $_REQUEST['end_time'];
}

$_REQUEST['checkpoint_id'] = issetVal( $_REQUEST['checkpoint_id'], '' );

if ( ! empty( $_REQUEST['checkpoint_id'] )
	&& ! DBGetOne( "SELECT ID
		FROM entry_exit_checkpoints
		WHERE ID='" . (int) $_REQUEST['checkpoint_id'] . "'" ) )
{
	$_REQUEST['checkpoint_id'] = '';
}

$_REQUEST['record_type'] = issetVal( $_REQUEST['record_type'], '' );
$_REQUEST['last_record'] = issetVal( $_REQUEST['last_record'], '' );
$_REQUEST['unauthorized'] = issetVal( $_REQUEST['unauthorized'], '' );

echo ErrorMessage( $note, 'note' );

echo ErrorMessage( $error );

// Advanced Search
if ( $_REQUEST['modfunc'] === 'search' )
{
	echo '<br />';

	$extra['new'] = true;

	$extra['search_title'] = _( 'Advanced' );

	$extra_action_url = PreparePHP_SELF( [], [ 'modfunc' ], [ 'advanced_search' => 1 ] );

	$extra['action'] = str_replace(
		'Modules.php?modname=' . $_REQUEST['modname'],
		'',
		$extra_action_url
	) . '&modfunc=&search_modfunc=';

	Search( 'student_id', $extra );
}
elseif ( ! $_REQUEST['modfunc'] )
{
	echo '<form action="' . PreparePHP_SELF(
		[],
		[
			'start_time',
			'start',
			'month_start',
			'day_start',
			'year_start',
			'end_time',
			'end',
			'month_end',
			'day_end',
			'year_end',
			'last_record',
			'unauthorized',
			'checkpoint_id',
			'record_type',
		]
	) . '" method="GET">';

	if ( ! AllowEdit() )
	{
		$tmp_allow_edit = true;

		$_ROSARIO['allow_edit'] = true;
	}

	DrawHeader( EntryExitRecordsStartEndDateTimeHeader() );

	DrawHeader(
		EntryExitRecordsCheckpointHeader(),
		EntryExitRecordsTypeHeader()
	);

	DrawHeader(
		EntryExitRecordsLastRecordHeader(),
		EntryExitRecordsUnauthorizedHeader()
	);

	// Advanced Search link.
	$advanced_link = ' <a href="' . PreparePHP_SELF( $_REQUEST, [ 'search_modfunc', 'include_inactive' ], [
		'modfunc' => 'search',
	] ) . '">' . _( 'Advanced' ) . '</a>';

	$records_sql = EntryExitStudentRecordsSQL( $start_datetime, $end_datetime );

	DrawHeader(
		! empty( $_ROSARIO['SearchTerms'] ) ? $_ROSARIO['SearchTerms'] : '',
		$advanced_link
	);

	if ( ! empty( $tmp_allow_edit ) )
	{
		$_ROSARIO['allow_edit'] = false;
	}

	echo '</form>';

	$columns = [];

	if ( AllowEdit()
		&& ! isset( $_REQUEST['_ROSARIO_PDF'] ) )
	{
		$columns += [ 'CHECKBOX' => MakeChooseCheckbox( '', 'ID', 'rec' ) ];
	}

	$columns += [ 'RECORDED_AT' => dgettext( 'Entry_Exit', 'Time' ) ];

	if ( ! $_REQUEST['checkpoint_id'] )
	{
		$columns += [ 'CHECKPOINT' => dgettext( 'Entry_Exit', 'Checkpoint' ) ];
	}

	if ( ! $_REQUEST['record_type'] )
	{
		$columns += [ 'TYPE' => dgettext( 'Entry_Exit', 'Type' ) ];
	}

	$columns += [ 'FULL_NAME' => _( 'Student' ) ];

	if ( $_REQUEST['checkpoint_id']
		&& $_REQUEST['record_type'] )
	{
		$columns += [ 'STUDENT_ID' => sprintf( _( '%s ID' ), Config( 'NAME' ) ) ];
	}

	$columns += [
		'GRADE_ID' => _( 'Grade Level' ),
		'UNAUTHORIZED' => _( 'Status' ),
		'COMMENTS' => _( 'Comments' ),
	];

	// SQL limit records to 1000.
	$records_RET = DBGet( $records_sql,
	[
		'CHECKBOX' => 'MakeChooseCheckbox',
		'RECORDED_AT' => 'EntryExitMakeDateTime',
		'TYPE' => 'EntryExitRecordsMakeType',
		'FULL_NAME' => 'makePhotoTipMessage',
		'GRADE_ID' => 'GetGrade',
		'UNAUTHORIZED' => 'EntryExitRecordsMakeUnauthorized',
		'COMMENTS' => 'EntryExitRecordsMakeComments',
	] );

	$options['pagination'] = true;

	echo '<form method="POST" action="' . PreparePHP_SELF( [], [], [ 'modfunc' => 'delete' ] ) . '">';

	ListOutput(
		$records_RET,
		$columns,
		dgettext( 'Entry_Exit', 'Record' ),
		dgettext( 'Entry_Exit', 'Records' ),
		[],
		[],
		$options
	);

	if ( AllowEdit()
		&& $records_RET )
	{
		echo '<br /><div class="center">' .
			SubmitButton( dgettext( 'Entry_Exit', 'Delete the selected Records' ), '', '' ) . '</div>';
	}

	echo '</form>';
}
