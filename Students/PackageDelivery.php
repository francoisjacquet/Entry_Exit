<?php
/**
 * Package Delivery (Students)
 *
 * URL to
 * - `&modfunc=save` = Save
 * - `&comments=My comment` = Comments "My comment"
 * - `&st[]=__STUDENT_ID__` = Student ID
 * - now
 * @example Modules.php?modname=Entry_Exit/PackageDelivery.php&modfunc=save&comments=My comment&st[]=__STUDENT_ID__
 *
 * @package Entry and Exit module
 */

if ( $_REQUEST['modfunc'] === 'save' )
{
	$delivered_at = RequestedDate( 'delivered_at', '' );

	if ( empty( $delivered_at ) )
	{
		$delivered_at = DBDate();

		$_REQUEST['delivered_at_time'] = date( 'H:i' );
	}

	if ( empty( $_REQUEST['delivered_at_time'] )
		|| strlen( $_REQUEST['delivered_at_time'] ) < 5 ) // 12:56
	{
		$error[] = _( 'Please fill in the required fields' );
	}

	$delivered_at .= ' ' . $_REQUEST['delivered_at_time'];

	if ( empty( $_REQUEST['st'] ) )
	{
		$error[] = _( 'You must choose at least one student.' );
	}

	if ( ! $error )
	{
		foreach ( (array) $_REQUEST['st'] as $student_id )
		{
			if ( (int) $student_id < 1 )
			{
				continue;
			}

			$inserted = DBInsert(
				'entry_exit_package_deliveries',
				[
					'STUDENT_ID' => (int) $student_id,
					'COMMENTS' => issetVal( $_REQUEST['comments'], '' ),
					'DELIVERED_AT' => $delivered_at,
				]
			);

			if ( $inserted )
			{
				$program_user_config = ProgramUserConfig( 'EntryExitStudent', ( $student_id * -1 ) );

				if ( ! empty( $program_user_config['HAS_PACKAGE_TO_PICKUP'] ) )
				{
					// Unset "Has a package to pickup" checkbox.
					ProgramUserConfig(
						'EntryExitStudent',
						( $student_id * -1 ),
						['HAS_PACKAGE_TO_PICKUP' => '' ]
					);
				}
			}
		}

		if ( ! empty( $inserted ) )
		{
			$note[] = button( 'check' ) . '&nbsp;' . dgettext( 'Entry_Exit', 'Packages were delivered for the selected students.' );
		}
	}

	// Unset modfunc, st & redirect URL.
	RedirectURL( [ 'modfunc', 'st' ] );
}

echo ErrorMessage( $note, 'note' );

echo ErrorMessage( $error );

if ( ! $_REQUEST['modfunc'] )
{
	$extra['link'] = [ 'FULL_NAME' => false ];
	$extra['SELECT'] = ",NULL AS CHECKBOX";

	if ( $_REQUEST['search_modfunc'] === 'list' )
	{
		echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=save' ) . '" method="POST">';

		DrawHeader( '', SubmitButton( dgettext( 'Entry_Exit', 'Deliver Package to Selected Students' ) ) );

		echo '<br />';

		EntryExitPackageDeliveryPopTableOutput();

		echo '<br />';
	}

	$extra['functions'] = [ 'CHECKBOX' => 'MakeChooseCheckbox' ];

	$extra['columns_before'] = [
		'CHECKBOX' => MakeChooseCheckbox( 'required', 'STUDENT_ID', 'st' ),
	];

	$extra['new'] = true;

	EntryExitPackageDeliveryWidget();

	Search( 'student_id', $extra );

	if ( $_REQUEST['search_modfunc'] === 'list' )
	{
		echo '<br /><div class="center">' .
			SubmitButton( dgettext( 'Entry_Exit', 'Deliver Package to Selected Students' ) ) . '</div></form>';
	}
}
