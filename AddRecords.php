<?php
/**
 * Add Records
 *
 * @package Entry and Exit module
 */

require_once 'modules/Entry_Exit/includes/common.fnc.php';
require_once 'modules/Entry_Exit/includes/AddRecords.fnc.php';

if ( empty( $_SESSION['Entry_Exit_type'] ) )
{
	$_SESSION['Entry_Exit_type'] = 'student';
}

if ( ! empty( $_REQUEST['type'] ) )
{
	$_SESSION['Entry_Exit_type'] = $_REQUEST['type'];
}
else
{
	$_REQUEST['type'] = $_SESSION['Entry_Exit_type'];
}

$header = '<a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&type=student' ) . '">' .
	( $_REQUEST['type'] === 'student' ?
	'<b>' . _( 'Students' ) . '</b>' : _( 'Students' ) ) . '</a>';

$header .= ' | <a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&type=staff' ) . '">' .
	( $_REQUEST['type'] === 'staff' ?
	'<b>' . _( 'Users' ) . '</b>' : _( 'Users' ) ) . '</a>';

DrawHeader(  ( $_REQUEST['type'] == 'staff' ? _( 'User' ) : _( 'Student' ) ) . ' &minus; ' . ProgramTitle() );
User( 'PROFILE' ) === 'student' ? '' : DrawHeader( $header );

require_once 'modules/Entry_Exit/' . ( $_REQUEST['type'] == 'staff' ? 'Users' : 'Students' ) . '/AddRecords.php';
