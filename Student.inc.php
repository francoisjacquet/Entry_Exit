<?php
/**
 * Entry and Exit Student Info tab
 *
 * Has a package to pick-up checkbox
 * Evening Leaves list
 *
 * @package Entry and Exit module
 */

require_once 'modules/Entry_Exit/includes/common.fnc.php';
require_once 'modules/Entry_Exit/includes/EveningLeave.fnc.php';

if ( AllowEdit() )
{
	if ( isset( $_REQUEST['program_user_config']['HAS_PACKAGE_TO_PICKUP'] ) )
	{
		ProgramUserConfig(
			'EntryExitStudent',
			( UserStudentID() * -1 ),
			[ 'HAS_PACKAGE_TO_PICKUP' => $_REQUEST['program_user_config']['HAS_PACKAGE_TO_PICKUP'] ]
		);
	}

	if ( isset( $_REQUEST['program_user_config']['PRIVATE_NOTES'] ) )
	{
		ProgramUserConfig(
			'EntryExitStudent',
			( UserStudentID() * -1 ),
			[ 'PRIVATE_NOTES' => $_REQUEST['program_user_config']['PRIVATE_NOTES'] ]
		);
	}

	if ( ! empty( $_REQUEST['values'] ) )
	{
		// Add eventual Dates to $_REQUEST['values'].
		AddRequestedDates( 'values' );

		foreach ( $_REQUEST['values'] as $id => $columns )
		{
			if ( ! empty( $columns['FROM_DATE_SINGLE'] ) )
			{
				// Evening Leave (single day).
				$columns['TO_DATE'] = $columns['FROM_DATE'] = $columns['FROM_DATE_SINGLE'];

				unset( $columns['FROM_DATE_SINGLE'] );

				$day_of_week = [
					0 => 'U',
					1 => 'M',
					2 => 'T',
					3 => 'W',
					4 => 'H',
					5 => 'F',
					6 => 'S',
				];

				// Deduce day of week based on date.
				$columns['DAYS_OF_WEEK'] = $day_of_week[ date( 'w', strtotime( $columns['FROM_DATE'] ) ) ];
			}

			if ( $id === 'new' )
			{
				// Check required columns on INSERT.
				if ( empty( $columns['CHECKPOINT_ID'] )
					|| empty( $columns['FROM_DATE'] )
					|| empty( $columns['TO_DATE'] )
					|| empty( $columns['DAYS_OF_WEEK'] )
					|| empty( $columns['RETURN_TIME'] ) )
				{
					continue;
				}
			}

			if ( ! empty( $columns['DAYS_OF_WEEK'] )
				&& is_array( $columns['DAYS_OF_WEEK'] ) )
			{
				$days = '';

				foreach ( (array) $columns['DAYS_OF_WEEK'] as $day => $y )
				{
					if ( $y == 'Y' )
					{
						$days .= $day;
					}
				}

				$columns['DAYS_OF_WEEK'] = $days;

				if ( empty( $columns['DAYS_OF_WEEK'] ) )
				{
					continue;
				}
			}

			$where_columns = [ 'SYEAR' => UserSyear(), 'STUDENT_ID' => UserStudentID() ];

			if ( $id !== 'new' )
			{
				$where_columns['ID'] = $id;
			}
			else
			{
				$columns['CREATED_BY'] = User( 'NAME' );
			}

			DBUpsert(
				'entry_exit_evening_leaves',
				$columns,
				$where_columns,
				( $id === 'new' ? 'insert' : 'update' )
			);
		}
	}

	// Unset values, program_user_config & redirect.
	RedirectURL( [ 'values', 'program_user_config' ] );
}

if ( $_REQUEST['modfunc'] === 'delete_evening_leave'
	&& AllowEdit() )
{
	if ( ! isset( $_REQUEST['delete_ok'] ) )
	{
		echo '</form>';
	}

	if ( DeletePrompt( dgettext( 'Entry_Exit', 'Evening Leave' ) ) )
	{
		DBQuery( "DELETE FROM entry_exit_evening_leaves
			WHERE ID='" . (int) $_REQUEST['id'] . "'" );

		// Unset modfunc & ID & redirect URL.
		RedirectURL( [ 'modfunc', 'id' ] );
	}
}

if ( ! $_REQUEST['modfunc'] )
{
	$program_user_config = ProgramUserConfig( 'EntryExitStudent', ( UserStudentID() * -1 ) );

	// Fix CSS responsive List width: do NOT use the .fixed-col class, use pure CSS.
	echo '<table class="width-100p valign-top" style="table-layout: fixed;"><tr><td>';

	$tooltip = '';

	if ( AllowEdit() )
	{
		$tooltip = '<div class="tooltip"><i>' . dgettext(
			'Entry_Exit',
			'Leave empty if no packages to pickup'
		) . '</i></div>';
	}

	$has_package_input = TextInput(
		issetVal( $program_user_config['HAS_PACKAGE_TO_PICKUP'], '' ),
		'program_user_config[HAS_PACKAGE_TO_PICKUP]',
		dgettext( 'Entry_Exit', 'Has a package to pickup' ) . $tooltip,
		'size="15" maxlength="50"'
	);

	DrawHeader( $has_package_input );

	// Notes about student (admin & teacher only).
	if ( User( 'PROFILE' ) !== 'student'
		&& User( 'PROFILE' ) !== 'parent' )
	{
		$notes_input = TextInput(
			issetVal( $program_user_config['PRIVATE_NOTES'], '' ),
			'program_user_config[PRIVATE_NOTES]',
			dgettext( 'Entry_Exit', 'Private Notes' ),
			'size="25" maxlength="255"'
		);

		DrawHeader( $notes_input );
	}

	// Display Evening Leave (several days) list.
	EntryExitEveningLeaveSeveralDaysListOutput( UserStudentID() );

	// Display Evening Leave (single day) list.
	EntryExitEveningLeaveSingleDayListOutput( UserStudentID() );

	// @since 4.3 Action hook.
	do_action( 'Entry_Exit/Student.inc.php|list_after' );

	echo '</td></tr></table>';
}
