<?php
/**
 * Mass Add Evening Leaves
 *
 * @package Entry and Exit module
 */

require_once 'modules/Entry_Exit/includes/common.fnc.php';
require_once 'modules/Entry_Exit/includes/EveningLeave.fnc.php';

if ( User( 'PROFILE' ) === 'teacher' )
{
	$_ROSARIO['allow_edit'] = true;
}

DrawHeader( ProgramTitle() );

if ( $_REQUEST['modfunc'] === 'save'
	&& AllowEdit() )
{
	if ( empty( $_REQUEST['st'] ) )
	{
		$error[] = _( 'You must choose at least one student.' );
	}

	if ( ! empty( $_REQUEST['values']['DAYS_OF_WEEK'] ) )
	{
		$days = '';

		foreach ( (array) $_REQUEST['values']['DAYS_OF_WEEK'] as $day => $y )
		{
			if ( $y == 'Y' )
			{
				$days .= $day;
			}
		}

		$_REQUEST['values']['DAYS_OF_WEEK'] = $days;
	}

	AddRequestedDates( 'values' );

	// Check required columns on INSERT.
	if ( empty( $_REQUEST['values']['CHECKPOINT_ID'] )
		|| empty( $_REQUEST['values']['FROM_DATE'] )
		|| empty( $_REQUEST['values']['TO_DATE'] )
		|| empty( $_REQUEST['values']['DAYS_OF_WEEK'] )
		|| empty( $_REQUEST['values']['RETURN_TIME'] ) )
	{
		$error[] = _( 'Please fill in the required fields' );
	}

	if ( ! $error )
	{
		foreach ( (array) $_REQUEST['st'] as $student_id )
		{
			if ( (int) $student_id < 1 )
			{
				continue;
			}

			if ( ! empty( $_REQUEST['delete_existing_evening_leave'] ) )
			{
				DBQuery( "DELETE FROM entry_exit_evening_leaves
					WHERE CHECKPOINT_ID='" . (int) $_REQUEST['values']['CHECKPOINT_ID'] . "'
					AND STUDENT_ID='" . (int) $student_id . "'
					AND SYEAR='" . UserSyear() . "'" );
			}

			$inserted = DBInsert(
				'entry_exit_evening_leaves',
				[
					'STUDENT_ID' => (int) $student_id,
					'SYEAR' => UserSyear(),
					'CREATED_BY' => User( 'NAME' ),
				] + $_REQUEST['values']
			);
		}

		if ( ! empty( $inserted ) )
		{
			$note[] = button( 'check' ) . '&nbsp;' . dgettext( 'Entry_Exit', 'Evening Leave was added to the selected students.' );
		}
	}

	// Unset modfunc, values, st & redirect URL.
	RedirectURL( [ 'modfunc', 'values', 'st' ] );
}

echo ErrorMessage( $note, 'note' );

echo ErrorMessage( $error );

if ( ! $_REQUEST['modfunc'] )
{
	$extra['link'] = [ 'FULL_NAME' => false ];
	$extra['SELECT'] = ",NULL AS CHECKBOX";

	if ( $_REQUEST['search_modfunc'] === 'list' )
	{
		echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=save' ) . '" method="POST">';

		DrawHeader( '', SubmitButton( dgettext( 'Entry_Exit', 'Add Evening Leave to Selected Students' ) ) );

		echo '<br />';

		EntryExitEveningLeavePopTableOutput();

		echo '<br />';
	}

	$extra['functions'] = [ 'CHECKBOX' => 'MakeChooseCheckbox' ];

	$extra['columns_before'] = [
		'CHECKBOX' => MakeChooseCheckbox( 'required', 'STUDENT_ID', 'st' ),
	];

	$extra['new'] = true;

	EntryExitEveningLeaveWidget();

	Search( 'student_id', $extra );

	if ( $_REQUEST['search_modfunc'] === 'list' )
	{
		echo '<br /><div class="center">' .
			SubmitButton( dgettext( 'Entry_Exit', 'Add Evening Leave to Selected Students' ) ) . '</div></form>';
	}
}
