<?php
/**
 * Module Functions
 * (Loaded on each page)
 *
 * @package Entry and Exit module
 */


/**
 * Entry and Exit module Portal Alerts.
 * "You have a package to pickup" (student) note.
 * "Your child has a package to pickup" (parent) note.
 *
 * @uses misc/Portal.php|portal_alerts hook
 *
 * @return true if new note, else false.
 */
function EntryExitPortalAlerts()
{
	global $note;

	if ( User( 'PROFILE' ) !== 'student'
		&& User( 'PROFILE' ) !== 'parent' )
	{
		return false;
	}

	$program_user_config = ProgramUserConfig( 'EntryExitStudent', ( UserStudentID() * -1 ) );

	$has_package_to_pickup = issetVal( $program_user_config['HAS_PACKAGE_TO_PICKUP'], '' );

	if ( $has_package_to_pickup )
	{
		if ( User( 'PROFILE' ) === 'student' )
		{
			$note_text = dgettext( 'Entry_Exit', 'You have a package to pickup' );
		}
		else
		{
			$note_text = dgettext( 'Entry_Exit', 'Your child has a package to pickup' );
		}

		$note_text .= ' &mdash; "' . $has_package_to_pickup . '"';

		// Add note.
		$note[] = '<img src="modules/Entry_Exit/icon.png" class="button bigger" /> ' . $note_text;

		return true;
	}

	return false;
}

add_action( 'misc/Portal.php|portal_alerts', 'EntryExitPortalAlerts', 0 );
