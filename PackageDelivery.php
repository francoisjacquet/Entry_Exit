<?php
/**
 * Package Delivery
 *
 * @package Entry and Exit module
 */

require_once 'modules/Entry_Exit/includes/common.fnc.php';
require_once 'modules/Entry_Exit/includes/PackageDelivery.fnc.php';

DrawHeader( ProgramTitle() );

$_REQUEST['view'] = issetVal( $_REQUEST['view'], '' );

if ( ! AllowEdit() )
{
	$_REQUEST['view'] = 'history';
}
else
{
	$view_link = 'Modules.php?modname=' . $_REQUEST['modname'] . '&view=';

	$view_select = SelectInput(
		$_REQUEST['view'],
		'view',
		'',
		[
			'' => dgettext( 'Entry_Exit', 'Package Delivery' ),
			'history' => dgettext( 'Entry_Exit', 'Package Deliveries History' ),
		],
		false,
		'autocomplete="off" ' .
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			// Note: `this.value` inside link is automatically replaced
			'class="onchange-ajax-link" data-link="' . URLEscape( $view_link ) . 'this.value"' :
			'onchange="' . AttrEscape( 'ajaxLink(' . json_encode( $view_link ) . ' + this.value);' ) . '"' ),
		false
	);

	DrawHeader( $view_select );
}

if ( $_REQUEST['view'] === 'history' )
{
	require_once 'modules/Entry_Exit/includes/PackageDeliveryHistory.php';
}
else
{
	require_once 'modules/Entry_Exit/Students/PackageDelivery.php';
}
