Entry and Exit module
=====================

![screenshot](https://gitlab.com/francoisjacquet/Entry_Exit/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/modules/entry-exit/

Version 4.5 - February, 2025

License GNU/GPLv2 or later

Author François Jacquet

Sponsored by AT group, Slovenia

DESCRIPTION
-----------
Entry and Exit module for RosarioSIS. Record coming and going of students and users at determined checkpoints. Track package deliveries.

Each checkpoint can have authorized times and can be used to record both entries and exits, or only entries, or only exits.

Easily add records for a checkpoint for one or various students or users at the same time.

Consult records, filter them by date, checkpoint or status. Limit the list to the last record only, or to a subgroup of students / users using the "Advanced" link.

A new "Entry and Exit" tab is added to the _Student Info_ program. On this tab, you can check if the student has a package to pickup. A "You have a package to pickup" note is displayed to students and their parents when they login.

In this tab, you can also write private notes about student, such as what to check for (alcohol, drugs, etc) when at the checkpoint.

This tab also lists "Evening Leaves". Evening Leaves are a way to indicate a student is authorized to return after the configured authorized time for a checkpoint. An Evening Leave consist of a beginning and end date, on specific days of the week, and a return time. For example, a student is authorized to come back later to the dormitory on Mondays, as he has a sport training session.

An Evening Leave report lets you consult which students have not come back / are not gone yet. It will list students and their status based on the "Evening Leave" and "Authorized Return Time". Students without evening leave can be hidden.

Finally, a custom Entries Report (not activated by default) which offers a slight variant to the Evening Leave report, notably the delay and attendance teacher.

Translated in [French](https://www.rosariosis.org/fr/modules/entry-exit/), [Spanish](https://www.rosariosis.org/es/modules/entry-exit/) and Slovenian.

AUTOMATION
----------
In order to automatically perform actions, you can trigger the following URLs (logged in as an administrator).

**Add Records (Students)**:
```
Modules.php?modname=Entry_Exit/AddRecords.php&modfunc=save&checkpoint_id=1&record_type=in&unauthorized=auto&type=student&st[]=__STUDENT_ID__
```
**Add Records (Users)**:
```
Modules.php?modname=Entry_Exit/AddRecords.php&modfunc=save&checkpoint_id=1&record_type=in&unauthorized=auto&type=staff&st[]=__STAFF_ID__
```
Where
- `&modfunc=save` = Save
- `&checkpoint_id=1` = Checkpoint ID 1
- `&record_type=in` = Record Type (entry: `in`, exit: `out`)
- `&unauthorized=auto` = Unauthorized (authorized: [empty], unauthorized: `Y`, automatic: `auto`)
- `&type=student` = Type (students: `student`, users: `staff`)
- `&st[]=__STUDENT_ID__` = Student / User ID

**Package Delivery**:
```
Modules.php?modname=Entry_Exit/PackageDelivery.php&modfunc=save&comments=My comment&st[]=__STUDENT_ID__
```
Where
- `&modfunc=save` = Save
- `&comments=My comment` = Comments "My comment" (optional)
- `&st[]=__STUDENT_ID__` = Student ID

CONTENT
-------
Entry and Exit
- Records
- Evening Leave
- Add Records
- Package Delivery
- Checkpoints

Students
- Student Info: "Entry and Exit" tab

INSTALL
-------
Copy the `Entry_Exit/` folder (if named `Entry_Exit-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School > Configuration > Modules_ and click "Activate".

Requires RosarioSIS 11.4+
