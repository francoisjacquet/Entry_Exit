<?php
/**
 * Checkpoints
 *
 * @package Entry and Exit module
 */

require_once 'modules/Entry_Exit/includes/common.fnc.php';

DrawHeader( ProgramTitle() );

if ( $_REQUEST['modfunc'] === 'update' )
{
	if ( isset( $_REQUEST['config']['ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS'] )
		&& is_numeric( $_REQUEST['config']['ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS'] )
		&& AllowEdit() )
	{
		Config(
			'ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS',
			$_REQUEST['config']['ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS']
		);
	}

	if ( ! empty( $_REQUEST['values'] )
		&& ! empty( $_POST['values'] )
		&& AllowEdit() )
	{
		foreach ( (array) $_REQUEST['values'] as $id => $columns )
		{
			if ( ! empty( $columns['SORT_ORDER'] )
					&& ! is_numeric($columns['SORT_ORDER'] ) )
			{
				// Fix SQL bug invalid numeric data.
				$error[] = _( 'Please enter valid Numeric data.' );

				continue;
			}

			if ( ( ! empty( $columns['AUTHORIZED_FROM'] )
					&& empty( $columns['AUTHORIZED_TO'] ) )
				|| ( ! empty( $columns['AUTHORIZED_TO'] )
					&& empty( $columns['AUTHORIZED_FROM'] ) ) )
			{
				// From or To time not set, error.
				$error[] = dgettext( 'Entry_Exit', 'Please enter both the "Authorized From" and the "Authorized To" times.' );

				continue;
			}

			if ( ! empty( $columns['AUTHORIZED_FROM'] )
				&& ! empty( $columns['AUTHORIZED_TO'] )
				&& $columns['AUTHORIZED_TO'] < $columns['AUTHORIZED_FROM'] )
			{
				// To time < From time, error.
				/*$error[] = dgettext( 'Entry_Exit', 'Please enter an "Authorized To" time posterior to the "Authorized From" time.' );

				continue;*/
			}

			if ( $id !== 'new' )
			{
				DBUpdate(
					'entry_exit_checkpoints',
					$columns,
					[ 'ID' => (int) $id ]
				);
			}
			// New: check for Title.
			elseif ( $columns['TITLE'] )
			{
				DBInsert(
					'entry_exit_checkpoints',
					$columns
				);
			}
		}
	}

	// Unset modfunc, values, config & redirect.
	RedirectURL( [ 'modfunc', 'values', 'config' ] );
}

if ( $_REQUEST['modfunc'] === 'remove'
	&& AllowEdit() )
{
	if ( DeletePrompt( dgettext( 'Entry_Exit', 'Checkpoint' ) ) )
	{
		DBQuery( "DELETE FROM entry_exit_checkpoints
			WHERE ID='" . (int) $_REQUEST['id'] . "'" );

		// Unset modfunc & ID & redirect.
		RedirectURL( [ 'modfunc', 'id' ] );
	}
}

if ( count( $_REQUEST ) === 3
	&& ! $_REQUEST['modfunc']
	&& Config( 'ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS' ) )
{
	// Only requested modname.
	$days_ago_date = date(
		'Y-m-d',
		strtotime( ( (int) Config( 'ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS' ) ) . ' days ago' )
	);

	// Automatically delete records older than X days
	DBQuery( "DELETE FROM entry_exit_student_records
		WHERE RECORDED_AT<'" . $days_ago_date . "';
		DELETE FROM entry_exit_staff_records
		WHERE RECORDED_AT<'" . $days_ago_date . "';" );
}

echo ErrorMessage( $error );

if ( ! $_REQUEST['modfunc'] )
{
	$checkpoints_RET = DBGet( "SELECT ID,TITLE,SORT_ORDER,
		TYPE,AUTHORIZED_FROM,AUTHORIZED_TO,
		(SELECT 1 FROM entry_exit_student_records stur,entry_exit_staff_records star
			WHERE stur.CHECKPOINT_ID=eec.ID
			OR star.CHECKPOINT_ID=eec.ID
			LIMIT 1) AS REMOVE
		FROM entry_exit_checkpoints eec
		ORDER BY SORT_ORDER IS NULL,SORT_ORDER,TITLE",
	[
		'REMOVE' => '_makeRemoveButton',
		'TITLE' => '_makeTextInput',
		'SORT_ORDER' => '_makeTextInput',
		'TYPE' => '_makeTypeInput',
		'AUTHORIZED_FROM' => '_makeTimeInput',
		'AUTHORIZED_TO' => '_makeTimeInput',
	] );

	$columns = [];

	if ( empty( $_REQUEST['LO_save'] ) )
	{
		// Do not Export Delete column.
		$columns['REMOVE'] = '<span class="a11y-hidden">' . _( 'Delete' ) . '</span>';
	}

	$columns += [
		'ID' => _( 'ID' ),
		'TITLE' => _( 'Title' ),
		'SORT_ORDER' => _( 'Sort Order' ),
		'TYPE' => dgettext( 'Entry_Exit', 'Type' ),
		'AUTHORIZED_FROM' => dgettext( 'Entry_Exit', 'Authorized From' ),
		'AUTHORIZED_TO' => dgettext( 'Entry_Exit', 'Authorized To' ),
	];

	$link['add']['html'] = [
		'REMOVE' => _makeRemoveButton( '', 'REMOVE' ),
		'TITLE' => _makeTextInput( '', 'TITLE' ),
		'SORT_ORDER' => _makeTextInput( '', 'SORT_ORDER' ),
		'TYPE' => _makeTypeInput( '', 'TYPE' ),
		'AUTHORIZED_FROM' => _makeTimeInput( '', 'AUTHORIZED_FROM' ),
		'AUTHORIZED_TO' => _makeTimeInput( '', 'AUTHORIZED_TO' ),
	];

	echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=update' ) . '" method="POST">';

	DrawHeader( '', SubmitButton() );

	$delete_input = '<table class="cellspacing-0"><tr><td>' . sprintf(
		dgettext( 'Entry_Exit', 'Automatically delete records older than %s days' ),
		'</td><td>' . TextInput(
			Config( 'ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS' ),
			'config[ENTRY_EXIT_DELETE_RECORDS_OLDER_THAN_DAYS]',
			'',
			'type="number" min="0" max="9999"'
		) . '</td><td>'
	) . '</td></tr></table>';

	DrawHeader( $delete_input );

	ListOutput(
		$checkpoints_RET,
		$columns,
		dgettext( 'Entry_Exit', 'Checkpoint' ),
		dgettext( 'Entry_Exit', 'Checkpoints' ),
		$link,
		[],
		[ 'valign-middle' => true ]
	);

	echo '<div class="center">' . SubmitButton() . '</div>';
	echo '</form>';
}


function _makeTextInput( $value, $name )
{
	global $THIS_RET;

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : 'new';

	$extra = 'maxlength=100';

	if ( $name === 'SORT_ORDER' )
	{
		$extra = ' type="number" min="-9999" max="9999"';
	}
	elseif ( $id !== 'new' )
	{
		$extra .= ' required';
	}

	return TextInput( $value, 'values[' . $id . '][' . $name . ']', '', $extra );
}


function _makeTypeInput( $value, $name )
{
	global $THIS_RET;

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : 'new';

	$options = [
		'' => dgettext( 'Entry_Exit', 'Entry and Exit' ),
		'in' => dgettext( 'Entry_Exit', 'Entry' ),
		'out' => dgettext( 'Entry_Exit', 'Exit' ),
	];

	return SelectInput(
		$value,
		'values[' . $id . '][' . $name . ']',
		'',
		$options,
		false
	);
}


function _makeTimeInput( $value, $name )
{
	global $THIS_RET;

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : 'new';

	$extra = 'type="time"';

	if ( $value )
	{
		$value = [
			// Strip seconds :00.
			mb_substr( $value, -3 ) === ':00' ? mb_substr( $value, 0, -3 ) : $value,
			EntryExitFormatTime( $value )
		];
	}

	return TextInput( $value, 'values[' . $id . '][' . $name . ']', '', $extra );
}


/**
 * Make Remove button
 *
 * Local function
 * DBGet() callback
 *
 * @param  string $value  Value.
 * @param  string $column Column name, 'REMOVE'.
 *
 * @return string Remove button or add button or none if existing Records for this Checkpoint.
 */
function _makeRemoveButton( $value, $column )
{
	global $THIS_RET,
		$checkpoints_RET;

	if ( empty( $THIS_RET['ID'] ) )
	{
		if ( empty( $checkpoints_RET ) )
		{
			return '';
		}

		return button( 'add' );
	}

	if ( $value )
	{
		// Do NOT remove existing Records for this Checkpoint.
		return '';
	}

	$button_link = 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=remove&id=' .
		$THIS_RET['ID'];

	return button( 'remove', '', URLEscape( $button_link ) );
}
